# Schematics and KiCAD drawings for the treadmill rig

A simplified view of the schematics, whose purpose is to illustrate how the PCBs work, is available [here](svg_files/pcb_arduino.sch.svg) for the Arduino Due Shield, and [here](svg_files/pcb_terminal.sch.svg) for the Terminal board.

## Arduino pinout
Arduino pin | PCB name
:---|:---|:
18 | enc1
17 | enc2
16 | photo
15 | valve1
7  | valve2
3  | lick1
4  | lick2
5  | gen1 - button
6  | gen2
14 | gen3
8  | gen4
9  | in1
10 | in2
11 | in3
12 | in4
13 | out1 - trigger
2  | out2
19 | out3

## Components

### List of components

#### Arduino Due Shield

Component|Quantity|RS Components Link
:---------| :-----:|-----:
Resistor 1k THT L6.3mm D2.3mm|27|[Link](https://benl.rs-online.com/web/p/through-hole-fixed-resistors/7398758/)
Resistor 2k THT L6.3mm D2.3mm |15|[Link](https://benl.rs-online.com/web/p/through-hole-fixed-resistors/0148578/)
Hex inverter SN74AC04NE4|2|[Link](https://benl.rs-online.com/web/p/hex-inverters/1450194/)
SMA connector right angle |7|[Link](https://benl.rs-online.com/web/p/sma-connectors/7839647/)
IDC 16 connector female THT |1|[Link](https://benl.rs-online.com/web/p/idc-connectors/6257268/)
LEDs |12 | 
Blue ||[Link](https://benl.rs-online.com/web/p/visible-leds/8622730/)
Green ||[Link](https://benl.rs-online.com/web/p/visible-leds/7082759/)
Red ||[Link](https://benl.rs-online.com/web/p/visible-leds/2285916/) 
Yellow ||[Link](https://benl.rs-online.com/web/p/visible-leds/8154372/)
Orange ||[Link](https://benl.rs-online.com/web/p/visible-leds/2285938/)
White ||[Link](https://benl.rs-online.com/web/p/visible-leds/7269287/)

#### Terminal

Component  | Quantity|RS Components Link 
:---------| :-----:|-----:
Resistor 1k THT L6.3mm D2.3mm |3|[Link](https://benl.rs-online.com/web/p/through-hole-fixed-resistors/7398758/) 
Resistor 2.7k THT L6.3mm D2.3mm |5|[Link](https://benl.rs-online.com/web/p/through-hole-fixed-resistors/1251161/)
Resistor 3.9k THT L6.3mm D2.3mm |2|[Link](https://benl.rs-online.com/web/p/through-hole-fixed-resistors/0148641/) 
Resistor 47k THT L6.3mm D2.3mm |1|[Link](https://benl.rs-online.com/web/p/through-hole-fixed-resistors/7078369/) 
Resistor 10M THT L6.3mm D2.3mm |2|[Link](https://benl.rs-online.com/web/p/through-hole-fixed-resistors/6832945/) 
Hex Schmitt Trigger Inverter SN74HCT14N |1|[Link](https://benl.rs-online.com/web/p/schmitt-trigger-inverters/0526331/) 
14 Pins DIP socket|1|[Link](https://benl.rs-online.com/web/p/dil-sockets/6742476/)
Transistor TIP120 |2|[Link](https://benl.rs-online.com/web/p/darlington-transistors/7743653/)
Transistor PN2222A |2|[Link](https://benl.rs-online.com/web/p/bipolar-transistors/7390555/)
Schottky diode |2|[Link](https://benl.rs-online.com/web/p/rectifier-schottky-diodes/6882280/)
Connector 2 way |3|[Link](https://benl.rs-online.com/web/p/pcb-terminal-blocks/7075458/)
Connector 3 way |6|[Link](https://benl.rs-online.com/web/p/pcb-terminal-blocks/7075452/)
Connector 5 way |1|[Link](https://benl.rs-online.com/web/p/pcb-terminal-blocks/7075464/)
IDC 16 connector female THT |1|[Link](https://benl.rs-online.com/web/p/idc-connectors/6257268/)
Capacitor 1uF |2|[Link](https://benl.rs-online.com/web/p/ceramic-multilayer-capacitors/8523306/)
Capacitor 0.1uF |2|[Link](https://benl.rs-online.com/web/p/ceramic-multilayer-capacitors/8523261/)
LEDs |1|

## How to print a PCB

Upload the .kicad_pcb file to [Eurocircuits](https://www.eurocircuits.com/).
The price per unit varies a lot with the volume.

## How to build the board

Take the PCBs and the components, then solder the components on the PCBs.
If you are a novice in soldering and electronics, the tips below should provide you all the informations you need.

###Soldering tips

* Keep the tip of the soldering iron clean at all times. Dirty tip means poor heat transfer.
* Put water on the sponge.
* If you have long air, attach them in your back.
* There are two types of solder for electronics: lead-based and lead-free. 
Lead-based solder can be bad for your health as lead is toxic, but lead does not go into the fumes below 450C, and this solder melting point is around 190C. 
So unless you put it in your mouth you will be fine... Still, avoid inhalating the fumes as the solder also contains flux, which is asthmagen. And clean your hands after touching solder.
Lead-free solder has a higher melting point at 220C and, as the name suggests, does not contain lead. However, it contains more flux in the fumes because the working temperature is higher.
So again, don't breathe the fumes.
In conclusion, lead-based solder is a bit easier to work with.
* Solder the components by order of size: first the small ones (resistors, diodes,...), and finish with the larger ones (pins, connectors,...).
* Bend both legs of the components before placing them. For the components that use little space on the board (like the resistors), use your thumb's nail to bend the legs right next to the body of the component.
* Insert the component in its PCB location with its body as close to the board as possible (except the 2N2222A transistors that should keep a small spacing to make sure that their metallic case does not scratch the PCB lines) and spread both its legs. Eventually use a plier to pull the body closer to the board.
To go faster, you can insert many components of the same type (for example, resistors) in one go before soldering them all.
* Some components have to be placed in a specific orientation: the LEDs, the ICs (hex inverter, schmitt trigger), the diodes, the IDC16 connectors, the transistors.
* In electronics, you are likely to encounter components that share the same package (physical envelop) but that contain completely different functions.
Example: both the LM7805 and the TIP120 have a TO-220 package and look exactly the same (except for the numbers), but one is a voltage regulator, the other is a transistor.
* Heat up the pads present on the back of the PCB (and not the legs of the components) and push the solder on the pad, close to the soldering iron tip (without touching it).
When the solder melts and fills the space between the pad and the leg the component is soldered to the board.
* When soldering to ground (GND), the heatsink might be large in some cases. Add a small drop of solder on the tip of the soldering iron to increase the heat transfer towards the pad and make the soldering easier.
* Once the component is soldered, cut the extra length from its legs, as close to the board as possible.
* To make sure that components with multiple pins (connectors, DIP sockets,...) are against the board, you can start by soldering one pin, then push the component close to the board while heating up that pin.
Finally, you can solder the other pins.
* The external pins of the SMA connectors are connected to the case, which is the GND, so do not touch the case with your hands when soldering those pins as it will get very hot.
* In some places on the PCB, a 2.5k value is given for the resistors. You can use a 2.4k or 2.7k resistor with the same effect.
* Do not solder the ICs directly to the board: use a 14 Pins DIP socket adapter.
* If you have a doubt on which component to solder, refer to the components lists for each PCB. If there is still a doubt, you can refer to the KiCad schematics.


## Other need to know

* The transistors 2N2222A are prone to dying without notice if you misuse them.
* The jumpers on the terminal board are meant to eventually connect another circuit, if the need arises. For the valves, it allows to eventually connect another voltage source.
* There are three GND pins on the IDC connectors and cable because there was no need for additional signal pins (and a GND failure would be an obvious source of problems).
* The capacitors are optional, but should protect the circuit from voltage fluctuations. 
The circuit was not meant to be bulletproof: there are very few to no fail-safes. So far there has been no need to implement any but that could eventually come in a future version.
Example: if you put the 12V on the GND and the GND on the 12V you are definitely going to damage components.



