#Linear treadmill rig

This is arduino and python code to interface with a linear treadmill. Can be used for logging of behavioural training and closed loop experimental control.

##How to perform experiments?
###Training
The application is split in a server-client architecture. 

The server controls the arduino, logger and experiment duration. 

The client sends orders to the server and receives realtime feedback on the experiment.

* Starting the server: treadmill-rig due -p <PORTNUMBER>
* Starting the client (and open browser): treadmill-rig remote -p <PORTNUMBER> --browser

You can close the browser when ever you want, the recording is only affected when you mess with the server.

* To close: Ctr+C on the server and the client terminal windows.

###Summary of training

* treadmill-rig summary -b

###Bash upload to multiple arduino boards:


for((i=0;i<6;i++));do arduino --upload due.ino --port /dev/ttyACM$i;done


##How to get set up?
###Hardware

Rig components [list](rig_components.pdf).

SVG [file](svg_files/) (done on [Inkscape](https://inkscape.org/en/release/0.92.3/)) for the laser cutted wheels and platform (laser cutted in the [FabLab Leuven](https://www.fablab-leuven.be/)).

(You might want to consider looking at the photos in [list](rig_components.pdf) to have a better idea of the structure.)

PCB [instructions](./pcb_readme.md)


####Structure

1. Start with the frame: one construction cube (RM1G) on each side of the long rails (XE25L500/M), and short rails (XE25CUSTRAIL/M, 150mm)for the "legs" of the frame. [See picture](drawings/1_structure.png)
Then connect those two assemblies together with 3 short rails (XE25CUSTRAIL/M, 150mm): 2 on floor level when the assemblies are "standing", 1 in the middle of the assemblies. They are fixed on the rails by table clamps (XE25CL2). [See picture](drawings/2_structure.png)
2. Add the supports for the wheels: 4 mounting bases (BA1S/M) parts (on top of each leg, placed in the direction of the rails) each supporting a post holder (PH50/M) containing an optical post of 75mm (TR75/M; for those I will use the abreviation). [See picture](drawings/3_structure.png)
3. Add the support for the platform: 2 mounting bases (BA1S/M) (perpendicular to the rails, outwards in regard to the structure) each supporting the equivalent of a TR250/M (we used a TR50/M + TR200/M).
Place the 3 right-angle clamps (RA90/M) on a TR200/M (we'll call this assembly A) and slide the ones on the sides on the TR250/M supports.
[See picture](drawings/4_structure.png)
Place 4 rotating clamps (SWC/M) on a TR200/M (we'll call this assembly B) and do the same, then place the two headpost clamps (4PC69) in the rotating clamps (SWC/M) in the middle. 
Note: it is floating in the global pictures because the Solidworks part of the rotating clamp (SWC/M; available on Thorlabs) is not articulated.
[See picture](drawings/5_structure.png)
4. The platform (see below) is fixed with a mounting base (BA1S/M) and a TR75/M on assembly A's middle right-angle clamp (RA90/M) (the screws go through the two holes; the rectangular window will be used for the photosensor).
[See picture](drawings/6_structure.png)
5. Adjust assembly A's height to be slightly higher than the wheels. Please note that assembly B can be inclined.
6. Support for the water sprout: place a TR150/M on top of a mounting base (BA1S/M), place a rotating clamp (SWC/M) on top, place the assembly on the left between the frontwheel and the support for the platform (the BA1S/M perpendicular and outwards in regard to the structure).
7. Support for the water dispenser: place the equivalent of what would be a TR350/M (we use TR200/M + TR150/M) on top of a mounting base (BA1S/M), on the rear left (the BA1S/M perpendicular and outwards in regard to the structure).
[See picture](drawings/7_structure.png)

####Platform

[See picture](drawings/wheels_platformedge.svg.png)

1. Glue the side panels (thin long bars in picture) on the thick part of the platform in the rear (which is the opposite of where the rectangular window is). 
2. When it is dry, glue the thin part of the platform.

####Wheels

[See picture](drawings/wheels_platformedge.svg.png)

1. The wheels are not perfectly symmetrical.
There is a tiny mark (circle) on each wheel, make them match and keep the right orientation (the faces of the wheels which had the white plastic membrane from the plexi should be facing the same direction).
2. Assemble the wheels: 2 faces, 8 bars. Place the wheel in an angle to be sure everything is well aligned!
3. Apply superglue on each bar-face contact. Wait for it to dry, then the other side.
4. Slide the shaft into the wheel, then a pillow block on each side of the shaft. Note: try not to force the shaft into the wheel, be gentle and rotate it while pushing. 
5. Screw the assembly to the rig with the M4x30mm screws.
6. Glue sections of sandpaper on the bars of the frontwheel to greatly increase the friction with the belt (which is very needed, since every belt slippage results in a decreased accuracy of the walked distance).

####Fixing the encoder

1. Put the shaft in the encoder and tighten it. Once the encoder moves with the wheel you are good and can remove the hex key.
2. Use hot glue to fix the encoder to the pillow block (make sure the wheel can turn smoothly without the encoder making noise).

Note: if you need to remove the encoder, simply turn the wheel while trying to insert the hex key. The hex key can only go in in a certain angle: it will block motion once it is in.

###Software
The due controller and the logger are independent processes from the main thread and update shared variables in the main object. Complex experiments can be designed by using the **BehaviorRig** from the **rig** module that incorporates both the logger and the arduino controller.

* Compilation of the *arduino due* code.

####Installation on the host computer
   - Dependencies: numpy matplotlib scipy pandas bokeh jupyter bokeh >12.5 pandas pyserial pyzmq jupyter

   - Install Miniconda
   - Install dependencies with conda install
   - Clone the repository (git clone <address> <targetfolder>)
   - Run python setup.py develop from the cloned directory
   
* Authenticating to the mouselab database [Not supported yet]

####Arduino commands

All (input and output) commands start with '@' and end with ETX.

####Input commands

* Start experiment
* Set reward size
* Give reward
* Set number of laps until reward

####Output commands

* Experiment started
* Position on lap
* Reward given
* Lap completed
* Photosensor pulse