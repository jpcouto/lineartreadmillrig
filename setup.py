#!/usr/bin/env python
# Install script for linearTreadmillRig.
# Joao Couto - March 2017

import os
from os.path import join as pjoin
from setuptools import setup,find_packages
from setuptools.command.install import install

longdescription = ''' Python tools for arduino communication and rig management.'''

setup(
    name = 'linearTreadmillRig',
    version = '0.1',
    author = 'Joao Couto',
    author_email = 'jpcouto@gmail.com',
    description = (longdescription),
    long_description = longdescription,
    license = 'GPL',
    packages = find_packages(),
    include_package_data=True,
    entry_points = {
        'console_scripts': [
            'treadmill-viewer = rig.widgets:main',
            'treadmill-rig = rig.server.launcher:main',
            'mousedb-auth = rig.google.gauth:main'
        ]
    }    
)
