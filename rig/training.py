from .rig import *

class Task(object):
    ''' Task on the linear treadmill, give conditional rewards '''
    def __init__(self, rig):
        self.rig = rig

    def reset(self):
        ''' Initialize task variables.'''
        pass
    
    def update(self):
        ''' Update loop. '''
        pass

class TaskRewardOnLick(Task):
    def __init__(self,**kwargs):
        pass

class TaskRewardOnLap(Task):
    def __init__(self,
                 rig,
                 *args,
                 **kwargs):
        Task.__init__(self,rig)
        self.reset()
    def reset(self):
        self.rig.setRelativePosition()
        self.rig.enableRewardOnLap()
        self.rig.setRewardOnLickDistance(-1)
    def update(self):
        pass

    
class TaskLickOnDistance(Task):
    def __init__(self,
                 rig,
                 rewardDistance = 200,
                 minRewardInterval = 30,
                 withTimedReward = True,
                 *args,
                 **kwargs):
        Task.__init__(self,rig)

        self.rewardDistance = int(rewardDistance)
        print('Setting reward distance: {0}'.format(self.rewardDistance))
        self.withTimedReward = withTimedReward
        self.minRewardInterval = minRewardInterval # in seconds
        self.reset()
    def reset(self):
        print('Disabled reward on lap')
        self.rig.disableRewardOnLap()
        self.rig.setRewardOnLickDistance(self.rewardDistance)
        self.lastRewardTime = self.rig.due.reward[0]/1000.
        self.lastLap = 0
        self.lastPosition = 0
        self.distance = 0
    def update(self):
        rtime = self.rig.getduinotime()
        if ((rtime - self.lastRewardTime) > self.minRewardInterval) and self.withTimedReward:
            self.rig.giveReward()
        self.lastRewardTime = self.rig.due.reward[0]/1000.
        '''
        // this is handled by the arduino now
        # So that it can be used in relative position mode
        if self.absoluteDistance:
            self.distance = self.rig.get_position()
        else:
            pos = self.rig.get_position()
            lap = self.rig.get_nLaps()
            if self.lastLap != lap:
                self.lastLap = lap
                self.distance += pos
            else:
                self.distance += pos - self.lastPosition
            self.lastPosition = pos
        # if we are in the right-ish distance
        if (self.distance - self.lastRewardDistance) > self.rewardDistance: 
            lickTime = self.rig.getLickTime()
            duetime = self.rig.due.duinotime.value/1000.
            # If the animal licked in the last second
            if (duetime - lickTime) < 1.:
                # if the last reward was a while ago
                if lickTime >= (self.lastRewardTime + self.minRewardInterval) : 
                    self.rig.giveReward()
                    self.lastRewardDistance = self.distance
                    self.lastRewardTime = lickTime
        '''
                    
    
class TrainingRig(BehaviorRig):
    def __init__(self,task = 'TaskRewardOnLap',**kwargs):
        BehaviorRig.__init__(self,**kwargs)
        self.initTask(task,**kwargs)
        
    def initTask(self,task,*args,**kwargs):
        task = str(task)
        if type(task) is str:
            if task.lower() == 'TaskLickOnDistance'.lower():
                self.task = TaskLickOnDistance(self,*args)
            elif task.lower() == 'TaskRewardOnLap'.lower():
                self.task = TaskRewardOnLap(self,*args)
            else:
                print("No task:")
                print(task)
                self.task = Task(self)
            self.task.reset()
        print('Initialized {task} task variables.'.format(task = task))
        
    def update(self):
        self.task.update()
        
