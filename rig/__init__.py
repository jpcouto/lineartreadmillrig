from .rig import BehaviorRig
from .utils import (parseBehaviorLog,
                    velocityFromPosition,
                    ticksToPosition,
                    listTrainingSessions,
                    getSessionsSummary)

from .training import TrainingRig

'''
Example:
    port = '/dev/ttyACM0'
    baudrate = 115200
    rig = BehaviorRig(port = port)

    rig.start_experiment()
    # Run for half hour
    
    for i in range(180):
        print('Position: '+ str(rig.get_position())+ " - nLaps: "+str(rig.get_nLaps()))
        time.sleep(10)
        
    rig.stop_experiment()
    rig.close()
    sys.exit()
'''
