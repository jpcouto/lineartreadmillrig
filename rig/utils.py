import numpy as np
from scipy.interpolate import interp1d
from glob import glob
from os.path import join as pjoin
import os
from datetime import datetime


def getCommitHash():
    import subprocess as sub
    codedir = os.path.dirname(os.path.abspath(__file__))
    try:
        commit_ref = sub.check_output(['git','rev-parse',
                                       '--verify','HEAD',
                                       '--short'],cwd = codedir).decode().strip('\n')
    except sub.CalledProcessError:
        print('''
        +++++++++++++++++++++++++++++++++++
        IMPORTANT TREADMILL RIG WARNING: 
        +++++++++++++++++++++++++++++++++++
    Could not fetch git commit hash!!! 
    You won't be able to retrieve the code version.
        
        Make sure to clone and install with "python setup.py develop"

    Don't run experiments like this!
        ++++++++++++++++++++++++++++++++++
    ''')
        commit_ref = None
    return commit_ref

RIG_CODE_VERSION = getCommitHash()

def parseLog(fname):
    commentstring = '#'
    LF = '\n'
    comments = []
    codesheader = '# CODES:'
    codesdict = {}
    with open(fname,'r') as f:
        for cmt in f:
            if cmt.startswith(codesheader):
                codes = cmt.strip(codesheader).strip(' ').strip('\n').split(',')
                for cd in codes:
                    name,code = cd.split('=')
                    codesdict[code] = name
    #print(str(codesdict))
    if not len(codesdict):
        raise "Could not read codes from: " + fname
    data = {}
    for k in codesdict.keys():
        data[codesdict[k]] = []
    
    with open(fname, "r") as f:
        for iLine,line in enumerate(f):
            if(not(line.startswith(commentstring)) and not(line.startswith(LF))):
                tmp =  line.strip('\r\n').split(',')

                data[codesdict[tmp[0]]].append([np.float(tmp[1])/1000.,
                                                np.float(tmp[2])/1000.,
                                                np.float(tmp[3])])
                
    for k in data.keys():
        data[k] = np.array(data[k])
    return comments,data

def parseBehaviorLog(fname,trimInconsistent=False):
    try:
        comments,data = parseLog(fname)
        encticks = data['position']
        lap = data['lap']
        reward = data['reward']
        lick = data['lick']
        pulses = {'screen':data['screen'],
                  'imaging':data['imaging'],
                  'cam1':data['cam1'],
                  'cam2':data['cam2']}
        #print("Encticks :")
        #print(str(encticks) + "\n")
    except Exception as ex:
        template = "An exception of type {0} occurred. Arguments:\n{1!r}"
        message = template.format(type(ex).__name__, ex.args)
        print(message)
        encticks = []
        lap = []
        reward = []
        lick = []
        pulses = {'screen':[],
                  'imaging':[],
                  'cam1':[],
                  'cam2':[]}
        commentstring = '#'
        comments = []
        with open(fname, "r") as f:
            for iLine,line in enumerate(f):
                if line.startswith(commentstring):
                    comments.append(line)
                else:
                    try:
                        tmp =  line.strip('\r\n').split(',')
                        if "P" in line:
                            encticks.append([np.float(tmp[0])/1000.,
                                             np.float(tmp[1])/1000.,tmp[3]])
                        elif "A" in line:
                            lick.append([np.float(tmp[0])/1000.,
                                         np.float(tmp[1])/1000.,tmp[3]])
                        elif "R" in line:
                            reward.append([np.float(tmp[0])/1000.,
                                           np.float(tmp[1])/1000.])
                        elif "L" in line:
                            lap.append([np.float(tmp[0])/1000.,
                                        np.float(tmp[1])/1000.,tmp[3]])
                            encticks.append([np.float(tmp[0])/1000.,
                                             np.float(tmp[1])/1000.,0])
                        elif "V" in line:
                            pulses['screen'].append([np.float(tmp[0])/1000.,
                                                     np.float(tmp[1])/1000.,tmp[3]])
                        elif "I" in line:
                            pulses['imaging'].append([np.float(tmp[0])/1000.,
                                                     np.float(tmp[1])/1000.,tmp[3]])
                        elif "E" in line:
                            pulses['cam1'].append([np.float(tmp[0])/1000.,
                                                   np.float(tmp[1])/1000.,tmp[3]])
                        elif "F" in line:
                            pulses['cam2'].append([np.float(tmp[0])/1000.,
                                                   np.float(tmp[1])/1000.,tmp[3]])
                    except:
                        print('Trouple with line [{0}]: {1}'.format(iLine,line))
                        continue
        
        encticks = np.array(encticks,dtype='float')
        lap = np.array(lap,dtype='float')
        reward = np.array(reward,dtype='float')
        lick = np.array(lick,dtype='float')
        pulses = dict(screen=np.array(pulses['screen'],dtype='float'),
                      imaging=np.array(pulses['imaging'],dtype='float'),
                      cam1=np.array(pulses['cam1'],dtype='float'),
                      cam2=np.array(pulses['cam2'],dtype='float'))
    
    if trimInconsistent:
        try:
            inconsistent = np.unique(np.hstack([0,np.where(np.diff(np.round(encticks[:,1]))<0)[0]+1,encticks.shape[0]]))
            idx = np.diff(inconsistent).argmax()
            minidx =  inconsistent[idx]
            maxidx =  inconsistent[idx+1]
            if not (minidx == 0) or not (maxidx == encticks.shape[0]):
                encticks = encticks[minidx:maxidx,:]
                print('Found an inconsistent log: '+os.path.basename(fname))
        except:
            pass
        # sort encoder ticks because i am adding the one for the lap
        try:
            encticks = encticks[encticks[:,1].argsort(),:]
        except:
            print('Failed reading.')

    return (encticks,lap,reward,lick,pulses,comments)


def velocityFromPosition(time,position):
    return np.diff(np.append(position,position[-1]))/np.diff(time[:2])

def ticksToPosition(timeticks,posticks,beltLength = 150,samplingRate = 100.):
    # Normalizes to position on belt and resets to lap. 
    position = posticks.copy()
    lapidx = np.where(np.diff(position)<-10)[0]
    lapidx = np.insert(lapidx,0,-1)
    averagelap = np.mean(position[lapidx])
    lapcount = 0
    for l in range(len(lapidx)-1):
        position[lapidx[l]+1:lapidx[l+1]+1] /= position[lapidx[l+1]]
        position[lapidx[l]+1:lapidx[l+1]+1] += lapcount
        lapcount += 1
    if not lapidx[-1] == len(position):
        position[lapidx[-1]+1:] /= averagelap
        position[lapidx[-1]+1:] += lapcount
        lapcount += 1
    position *= beltLength
    
    posfunc = interp1d(timeticks, position,
                       kind='linear',
                       bounds_error=False,
                       fill_value=np.min(position),
                       assume_sorted=True)
    x = np.arange(0,np.max(timeticks),1./samplingRate)
    y = posfunc(x)
    return x,y

def mplot_summary_position(time,position,lapticks,rewardticks,lickticks,beltLength=150):
    import pylab as plt

    plt.matplotlib.style.use('ggplot')
    fig = plt.figure()
    posax = fig.add_subplot(2,1,1)
    plt.plot(time,np.mod(position,beltLength))
    plt.vlines(rewardticks,beltLength,beltLength+4,color='r',lw = 1)
    plt.vlines(lapticks,beltLength+6,beltLength+10,color='g',lw = 1)
    #for i in range(lapcount):
    #    plt.hlines((i+1)*beltLength,0,x[-1],lw=0.3)
    plt.ylabel('Position (cm)')

    from scipy.signal import medfilt
    fig.add_subplot(2,1,2,sharex=posax )
    velocity = velocityFromPosition(time,position)
    plt.plot(time[:-1],medfilt(velocity,1),linewidth=0.2,color='k')
    plt.vlines(rewardticks[:,1],30,34,color='r',lw = 1)
    plt.vlines(lapticks[:,1],36,40,color='g',lw = 1)
    plt.hlines(y[-1]/x[-1],0,x[-1],linestyle='--',lw=0.6)
    plt.ylabel('Velocity (cm/s)')
    plt.xlabel('Time (s)')
    fig = plt.figure()
    posax = fig.add_subplot(1,2,1)
    velax = fig.add_subplot(1,2,2)
    for l in range(len(lapidx)-2):
        idx = np.where((x>encticks[lapidx[l+1],1]) & (x<=encticks[lapidx[l+2],1]))[0]
        posax.plot(x[idx] - x[idx[0]],y[idx] - y[idx[0]],lw=0.6,alpha=0.5)
        velax.plot(x[idx] - x[idx[0]],velocity[idx],lw=0.6,alpha=0.5)

def listTrainingSessions(path = pjoin(os.path.expanduser('~'),'data','training'),
                         exceptions=['summary','dummy','test'],logextension = 'behaviorlog'):
    if not os.path.isdir(path):
        print('Not a valid path: {0}'.format(path))
        raise
    folders = np.sort(glob(pjoin(path,'*')))
    for e in exceptions:
        folders = filter(lambda x: not e in x, folders)
    animals = [s for s in [os.path.basename(f) for f in folders]]
    sessions = {}
    for animal,folder in zip(animals,folders):
        sessions[animal] = {}
        sessions[animal]['paths'] = [f for f in np.sort(glob(pjoin(folder,'*.' + logextension)))]
        sessions[animal]['dates'] = []
        for log in sessions[animal]['paths']:
            try:
                sessions[animal]['dates'].append(datetime.strptime(
                    os.path.basename(log).split(animal)[0], '%Y%m%d_%H%M%S_'))
            except:
                print('Skipping '+ log)
                sessions[animal]['paths'].remove(log)
    return animals,sessions

def getSessionsSummary(animals,sessions,
                       threshold = 1,
                       datapath = pjoin(os.path.expanduser('~'),'data','training')):
    import pandas as pd
    summary = []
    for animal in animals:
        print('Analysing sessions from animal: ' + animal)
        summarypath = pjoin(datapath,'summary',animal+'.csv')
        if not os.path.isdir(os.path.dirname(summarypath)):
            os.makedirs(os.path.dirname(summarypath))
            print('Created summary directory.')
        if os.path.exists(summarypath):
            try:
                df = pd.read_csv(summarypath)
                df['date'] = pd.to_datetime(df['date'],
                                            format='%Y-%m-%d %H:%M:%S')
            except KeyError:
                print('Could not find date field.')
                df = pd.DataFrame()
        else:
            print('No summary for ' + animal)
            df = pd.DataFrame()
        for date,ses in zip(sessions[animal]['dates'],sessions[animal]['paths']):
            if len(df) and np.sum(df['file'] == os.path.basename(ses)) > 0:
                print(' --> Session '+os.path.basename(ses)+' is on file.')
                continue
            print(' -> Session ' + os.path.basename(ses))
            posticks,lap, reward,lick,pulses,comments = parseBehaviorLog(ses,trimInconsistent=True)
            try:
                time,position = ticksToPosition(posticks[:,1],posticks[:,2])
            except:
                print('Error reading: ' + ses)
                continue
            velocity = velocityFromPosition(time,position)
            
            dt = np.diff(time[:2])
            data = dict(date = date,
                        file = os.path.basename(ses),
                        duration = time[-1] - time[0],
                        nLaps = len(lap),
                        timeStill = np.sum(np.abs(velocity)<=threshold)*dt,
                        timeRunning = np.sum(velocity>threshold)*dt,
                        timeBackward = np.sum(velocity<-0.1)*dt,
                        velocityRunning = np.mean(velocity[velocity>threshold]),
                        velocityMean = np.mean(velocity))
            ndf = pd.DataFrame.from_dict(data)
            df = df.append(ndf,ignore_index=True)
        print('Updating ' + animal + ' summary.')
        df.to_csv(summarypath)
        summary.append(df)
    return summary
