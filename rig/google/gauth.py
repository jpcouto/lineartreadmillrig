from os.path import join as pjoin
import os

from oauth2client.client import OAuth2WebServerFlow
from oauth2client import tools 
from oauth2client.file import Storage

DEFAULTS_DIR = 'treadmillRig'

def get_gdocs_credentials(credential_dir = pjoin(
        os.path.expanduser('~'),DEFAULTS_DIR),
                          scopes =  ['https://spreadsheets.google.com/feeds'],
                         application_name = 'Mouselab database'):
    # This is adapted from google's examples.
    
    if not os.path.exists(credential_dir):
        os.makedirs(credential_dir)
        print('Created credential folder [{0}]'.format(credential_dir))

    store = Storage(pjoin(credential_dir,'mouselabdb_credentials.auth'))
    credentials = store.get()
    if credentials is None:
        flow = OAuth2WebServerFlow(
            client_id = '396787302422-mo3gsb9h0lnh73t7bhkb9bsd0e1hcihh.apps.googleusercontent.com',
            client_secret = 'kep2dFoJsnqwRdDuqcrjE_tx',
            scope = scopes,
            redirect_uri = 'http://example.com/auth_return')
        credentials = tools.run_flow(flow,store)
    return credentials

def main():
    # This script is usefull to get credentials from google docs
    credentials = get_gdocs_credentials()

if __name__ == '__main__':
    main()
    
