import gspread
from .gauth import get_gdocs_credentials

MOUSELAB_DATABASE= '1FSJln1CWbXRsuv8hNN1OUAEUNYoch_KXzHbYo53M3dE'
# The following does not need to be hardcoded...
# Read from first row later...
# Its like this because I disagree with the names of things.
LOG_STRUCTURE = ('date',
                 'mouseID',
                 'barcode',
                 'experimenter',
                 'time',
                 'paradigm',
                 'protocol',
                 'comments',
                 'waterVolume',
                 'animalWeight',
                 'duration',
                 'notes')

def getEntriesForAnimal(animalID,
                        dbID = MOUSELAB_DATABASE,
                        sheet=0,logStructure=LOG_STRUCTURE):
    credentials = get_gdocs_credentials()
    gc = gspread.authorize(credentials)
    if type(sheet) is int:
        log = gc.open_by_key(dbID).get_worksheet(sheet)
    elif type(sheet) is str:
        log = gc.open_by_key(dbID).worksheet(sheet)
    else:
        print('Unknown type for sheet input:'+str(type(sheet)))
        raise ValueError
    
    cells = log.findall(animalID)
    entries = {}
    for i,k in enumerate(logStructure):
        entries[k] = []
    print [log.row_values(c.row) for c in cell_list]
