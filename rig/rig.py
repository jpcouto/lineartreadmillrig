#! /usr/bin/env python
# Rig class to interface with the arduino controller.
#
import serial
import time
import sys
from multiprocessing import Process,Queue,Event,Value,Array
from ctypes import c_char_p,c_longdouble,c_long
import numpy as np
from math import pi
import re

from os.path import expanduser
homepath = expanduser("~")
import os
from os.path import join as pjoin
import csv
from datetime import datetime

from .utils import RIG_CODE_VERSION

import sys
if sys.version_info >= (3, 0):
    long = lambda x: int(x)

DEFAULTS_DIR = 'treadmillRig' # File is stored there and then copied to the output directory.

DUE_TIME_FACTOR = 1. # use DUE_TIME_FACTOR = 1000. if using "micros"
# ARDUINO MESSAGES
ERROR='E'
START = 'S'
STOP = 'Z'
ABSOLUTE_POS = 'X'
RELATIVE_POS  = 'B'
DISABLE_REWARD = 'D'
ENABLE_REWARD = 'Y'
RESET_POS = 'O'
SET_REWARD_SIZE = 'U'
SET_ACTUATOR_PARAMETERS = 'm' # takes actuator_npulses_width_interval
SET_REWARD_ON_LICK_DISTANCE = 'a' # activated reward on lick after some distance (on the arduino)
TIME = 'T'
OUT = 'O'
SCREEN = 'V'
IMAGING = 'I'
CAM1 = 'F'
CAM2 = 'G'
CAM3 = 'H'
SEP = '_'
NOW = 'N'
ACT0 = 'M'
ACT1 = 'Q'
STX='@'
ETX=serial.LF.decode()
LOGETX= '\n'
ENCODER = 'P'
REWARD = 'R'
LAP = 'L'
LICK = 'A'
# LOG MESSAGES
LOGSCREEN = 0
LOGIMAGING = 1
LOGPOSITION = 2
LOGLICK = 3
LOGREWARD = 4
LOGACT0 = 21
LOGACT1 = 22
LOGLAP = 5
LOGCAM1 = 6
LOGCAM2 = 7
LOGCAM3 = 8
LOGVSTIM = 10
RIGVERSION = 0.3

LOGCODES = 'screen={0},imaging={1},position={2},lick={3},reward={4},lap={5},cam1={6},cam2={7},cam3={8},act0={9},act1={10}'.format(
    LOGSCREEN,LOGIMAGING,LOGPOSITION,LOGLICK,LOGREWARD,LOGLAP,LOGCAM1,LOGCAM2,LOGCAM3,LOGACT0,LOGACT1)

# Class to talk to arduino due using a separate process.
class DueInterface(Process):
    def __init__(self,port='/dev/ttyACM0', baudrate=115200,
                 inQ = None, outQ=None,timeout=0.03):
        Process.__init__(self)
        self.inQ = inQ
        self.outQ = outQ
        self.port = port
        self.baudrate = baudrate
        self.timeout = timeout
        try:
            self.due = serial.Serial(port=self.port,
                                     baudrate=self.baudrate,
                                    timeout = self.timeout)
        except serial.serialutil.SerialException:
            raise(serial.serialutil.SerialException('''
Could not open DUE on port {0}

            Try logging out and back in.'''.format(self.port)))
        self.due.flushInput()
        self.due.close()
        self.display('Probed arduino on port {0}.'.format(port))

        self.corrupt_messages = 0
        self.exit = Event()        
        self.experiment_running = Event()        
        self.expStartTime   = Value(c_longdouble,time.time())
        self.duinotime   = Value(c_longdouble,0)
        self.position = Array(c_longdouble,[0,0])
        self.laps = Array(c_longdouble,[0,0])
        self.licks = Array(c_longdouble,[0,0])
        self.reward = Array(c_longdouble,[0,0])
        self.cam1 = Array(c_longdouble,[0,0])
        self.cam2 = Array(c_longdouble,[0,0])
        self.cam3 = Array(c_longdouble,[0,0])
        self.actuator0 = Array(c_longdouble,[0,0])
        self.actuator1 = Array(c_longdouble,[0,0])
        self.screen = Array(c_longdouble,[0,0])
        self.imaging = Array(c_longdouble,[0,0])

        #self.display('Due initialized values.')
    def close(self):
        self.stop_experiment()
        self.exit.set()

    def display(self,msg):
        print('['+datetime.today().strftime('%y-%m-%d %H:%M:%S')+'] - ' + msg)
        
    def due_write(self, data):
        self.due.write((STX + data + ETX).encode())
    
    def due_read(self):
        msg = self.due.readline()
        try:
            msg = msg.decode()
        except:
            self.display("WARNING: Decode failed for message: {0}".format(msg))
        return time.time(),msg

    def start_experiment(self):
        self.inQ.put(START)
        t = time.time()
        while not self.experiment_running.is_set():
            time.sleep(0.001)
        self.display('Software initiated experiment [latency: {0:.4f} s].'.format(
            time.time() - t))

    def stop_experiment(self):
        self.inQ.put(STOP)
        t = time.time()
        while self.experiment_running.is_set():
            time.sleep(0.001)
        self.display('Software stopped experiment [latency: {0:.4f} s].'.format(
            time.time() - t))

    def process_message(self, tread, msg):
        treceived = long((tread - self.expStartTime.value)*1000)
        #print(msg)
        if msg.startswith(STX) and msg[-1].endswith(ETX):
            msg = msg.strip(STX).strip(ETX)
            if msg[0] == ENCODER:
                tmp = msg.split(SEP)
                t = float(long(tmp[1]))/DUE_TIME_FACTOR
                self.position[0] = t
                self.position[1] = float(tmp[2])
                self.duinotime.value = t
                return [LOGPOSITION,treceived,t,int(tmp[2])]
            elif msg[0] == SCREEN:
                tmp = msg.split(SEP)
                t = float(long(tmp[1]))/DUE_TIME_FACTOR
                self.screen[0] = t
                self.screen[1] = float(tmp[2])
                self.duinotime.value = t
                return [LOGSCREEN,treceived,t,long(tmp[2])]
            elif msg[0] == IMAGING:
                tmp = msg.split(SEP)
                t = float(long(tmp[1]))/DUE_TIME_FACTOR
                self.duinotime.value = t
                self.imaging[0] = t
                self.imaging[1] = float(tmp[2]) 
                return [LOGIMAGING,treceived,t,long(tmp[2])]
            elif msg[0] == CAM1:
                tmp = msg.split(SEP)
                t = float(long(tmp[1]))/DUE_TIME_FACTOR
                self.duinotime.value = t
                self.cam1[0] = float(long(tmp[1]))/DUE_TIME_FACTOR
                self.cam1[1] = float(tmp[2])
                return [LOGCAM1,treceived,t,long(tmp[2])]
            elif msg[0] == CAM2:
                tmp = msg.split(SEP)
                t = float(long(tmp[1]))/DUE_TIME_FACTOR
                self.duinotime.value = t
                self.cam2[0] = t
                self.cam2[1] = float(tmp[2])
                return [LOGCAM2,treceived,t,long(tmp[2])]
            elif msg[0] == CAM3:
                tmp = msg.split(SEP)
                t = float(long(tmp[1]))/DUE_TIME_FACTOR
                self.duinotime.value = t
                self.cam3[0] = t
                self.cam3[1] = float(tmp[2])
                return [LOGCAM3,treceived,t,long(tmp[2])]
            elif msg[0] == REWARD:
                tmp = msg.split(SEP)
                t = float(long(tmp[1]))/DUE_TIME_FACTOR
                self.duinotime.value = t
                self.reward[0] = t
                self.reward[1] = self.position[1]
                return [LOGREWARD,treceived,t,0]
            elif msg[0] == ACT0:
                tmp = msg.split(SEP)
                t = float(long(tmp[1]))/DUE_TIME_FACTOR
                self.duinotime.value = t
                self.actuator0[0] = t
                self.actuator0[1] = float(tmp[2])
                return [LOGACT0,treceived,t,long(tmp[2])]
            elif msg[0] == ACT1:
                tmp = msg.split(SEP)
                t = float(long(tmp[1]))/DUE_TIME_FACTOR
                self.duinotime.value = t
                self.actuator1[0] = t
                self.actuator1[1] = float(tmp[2])
                return [LOGACT1,treceived,t,long(tmp[2])]

            elif msg[0] == LAP:
                tmp = msg.split(SEP)
                t = float(long(tmp[1]))/DUE_TIME_FACTOR
                self.duinotime.value = t
                self.laps[0] = t
                self.laps[1] = float(tmp[2])
                return [LOGLAP,treceived,t,long(tmp[2])]
            elif msg[0] == LICK:
                tmp = msg.split(SEP)
                t = float(long(tmp[1]))/DUE_TIME_FACTOR
                self.duinotime.value = t
                self.licks[0] = t
                self.licks[1] = float(tmp[2])
                return [LOGLICK,treceived,t,long(tmp[2])]
            elif msg[0] == START:
                self.duinotime.value = 0
                self.expStartTime.value = time.time()

                self.laps[:] = [0,0]
                self.licks[:] = [0,0]
                self.reward[:] = [0,0]
                self.cam1[:] = [0,0]
                self.cam2[:] = [0,0]
                self.cam3[:] = [0,0]
                self.actuator0[:] = [0,0]
                self.actuator1[:] = [0,0]
                self.screen[:] = [0,0]
                self.imaging[:] = [0,0]

                cnt = 0
                while not self.outQ.empty():
                    self.outQ.get()
                    cnt += 1
                print('Clearing queue to start experiment [{0} messages cleared].'.format(cnt))
                self.experiment_running.set()

                self.outQ.put('# RIG VERSION: {0}'.format(RIGVERSION))
                if not RIG_CODE_VERSION is None:
                    self.outQ.put('# RIG GIT COMMIT HASH: {0}'.format(
                        RIG_CODE_VERSION))
                self.outQ.put('# CODES: {0}'.format(LOGCODES))
                self.outQ.put('# RIG CSV: code,time received,duino time,value')
                return '# STARTED EXPERIMENT'
            elif msg[0] == STOP:
                self.experiment_running.clear()
                self.outQ.put('# STOPPED EXPERIMENT')
                return '# STOPPED EXPERIMENT'
            elif msg[0] == ENABLE_REWARD:
                self.outQ.put('# Enable reward set.')

            elif msg[0] == RESET_POS:
                tmp = msg.split(SEP)
                t = bool(tmp[1])
                self.outQ.put('# Reset position mode: {0}.'.format(int(t)))
                
            else:
                print('Unknown message: ' + msg)
        else:
            self.corrupt_messages += 1
            self.display('Error on msg [' + str(self.corrupt_messages) + ']: '+ msg.strip(STX).strip(ETX))
            return '# ERROR {0},{1}'.format(treceived,msg)

    def run(self):
        self.due = serial.Serial(port=self.port,
                                 baudrate=self.baudrate,
                                 timeout = self.timeout)
        self.due.flushInput()
        time.sleep(0.5) # So that the first cued orders get processed
        self.lastRewardPosition = 0
        while not self.exit.is_set():
            # Write whatever needed
            if not self.inQ.empty():
                message = self.inQ.get()
                self.due_write(message)
            # Read whatever is there
            if (self.due.inWaiting() > 0):
                tread,message = self.due_read()
                try:
                    toout = self.process_message(tread,message)
                except:
                    self.display('ERROR with: ' + message)
                if self.experiment_running.is_set():
                    self.outQ.put(toout)
                    
        self.due.close()
        self.display('Ended communication with arduino.')

# Logger class for behavioral data
# "#" are comments
# Log is organized by "time received, time arduino, type, value 0 ..."
# Type is the actual log
# "P" is position on track
# "R" is reward
# "L" is lap
# "A" is lick
class BehaviorLogger(Process):
    version = '0.1'
    def __init__(self,animalName='none',
                 logfolder = pjoin(homepath,'data','training'),
                 inQ = None, log = True):
        
        Process.__init__(self)
        self.animalName = Array('u',' '*64)
        self.logfolder = logfolder
        self.inQ = inQ
        self.logfile = None
        self.doLog = log
        self.exit = Event()
        self.isLogging = Event()
        self.setAnimalName(animalName)

    def display(self,msg):
        print('['+datetime.today().strftime('%y-%m-%d %H:%M:%S')+'] - ' + msg)

    def startlog(self):
        if self.isLogging.is_set():
            self.stoplog()
        self.isLogging.set()
    def stoplog(self):
        self.isLogging.clear()
        if not self.logfile is None:
            while not self.inQ.empty():
                tmp = self.inQ.get()
                if not tmp is None:
                    self.csvwriter.writerow(tmp)
                else:
                    break
            self.logfile.close()
            self.display('Closed logging to : ' + os.path.basename(
                self.filepath))
            animalName = self.getAnimalName()
            logfolder = os.path.abspath(pjoin(self.logfolder,animalName))
            if not os.path.isdir(logfolder):
                os.makedirs(logfolder)
            filepath = pjoin(logfolder,os.path.basename(self.filepath))
            os.rename(self.filepath,filepath)
            self.display('Moved file to: ' + logfolder)
            self.logfile = None
        
    def newLog(self):
        animalName = self.getAnimalName()
        logfolder = pjoin(os.path.expanduser('~'),DEFAULTS_DIR)
        if not os.path.isdir(logfolder):
            os.makedirs(logfolder)
        prefix = datetime.today().strftime('%Y%m%d_%H%M%S')
        
        filepath = pjoin(logfolder,prefix+'_'+animalName+'.behaviorlog')
        if not self.logfile is None:
            close(self.logfile)
        self.filepath = filepath
        self.logfile = open(filepath,'w')
        self.csvwriter = csv.writer(self.logfile, delimiter=',')
        self.display('Started logging to : ' + os.path.basename(filepath))
        
    def setAnimalName(self,animalName):
        self.stoplog()
        for i in range(len(self.animalName)):
            self.animalName[i] = ' '
        for i in range(len(animalName)):
            self.animalName[i] = animalName[i]
        self.display('Set animal [{0}]'.format(str(self.animalName[:]).strip(' ')))
        
    def getAnimalName(self):
        return str(self.animalName[:]).strip(' ')
        
    def run(self):
        while not self.exit.is_set():
            while self.isLogging.is_set():
                if self.logfile is None:
                    self.newLog()
                    self.logfile.write('# Behavioral log version ' + self.version + LOGETX)
                    self.logfile.write('# ' + datetime.today().strftime('%d-%m-%Y - %H:%M:%S') + LOGETX)           
                    self.logfile.write('# Animal: {0}'.format(self.getAnimalName()) + LOGETX)
                if not self.inQ.empty():
                    tmp = self.inQ.get()
                    if not tmp is None:
                        if type(tmp) is list:
                            self.csvwriter.writerow(tmp)
                        else:
                            if not tmp.startswith('#'):
                                tmp = '# ' + tmp
                            self.logfile.write(tmp + LOGETX)
            if not self.logfile is None:
                self.stoplog()
            time.sleep(0.05)

    def close(self):
        self.isLogging.clear()
        self.exit.set()

class BehaviorRig(object):
    def __init__(self,animalName = 'dummy', port = '/dev/ttyACM0',
                 baudrate = 115200, log = True,logger = None,resetLog = True):
        self.running_experiment = Event()
        self.port = port
        self.baudrate = baudrate
        self.ordersQ = Queue()
        self.resetLog = resetLog
        if logger is None: # This should not be essential.
            self.logQ = Queue()
            self.logger = BehaviorLogger(animalName = animalName,
                                         inQ = self.logQ, log = log)
            self.logger.daemon = True
            self.logger.start()

        else:
            self.logger = logger
            self.logQ = logger.inQ

        self.due = DueInterface(port, baudrate, inQ = self.ordersQ,
                                outQ = self.logQ)
        self.due.daemon = True
        self.due.start()

    def getduinotime(self):
        return self.due.duinotime.value/1000.
    def setRewardSize(self,reward_size):
        self.ordersQ.put(SET_REWARD_SIZE+SEP+str(int(reward_size)))

    def giveReward(self):
        self.ordersQ.put(REWARD)

    def activateActuator0(self):
        self.ordersQ.put(ACT0)
        
    def activateActuator1(self):
        self.ordersQ.put(ACT1)
    def setActuatorParameters(self,actuator = 0,npulses = 5,width = 10, period=90,position_trigger = -100):
        self.ordersQ.put(SET_ACTUATOR_PARAMETERS+'{0}{1}{0}{2}{0}{3}{0}{4}{0}{5}'.format(SEP,int(actuator),
                                                                                   int(npulses),
                                                                                   int(width),
                                                                                   int(period),
                                                                                   int(position_trigger)))
        print(SET_ACTUATOR_PARAMETERS+'{0}{1}{0}{2}{0}{3}{0}{4}{0}{5}'.format(SEP,int(actuator),
                                                                              int(npulses),
                                                                              int(width),
                                                                              int(period),
                                                                              int(position_trigger)))

    def setAnimalName(self, animalName):
        self.logger.setAnimalName(animalName)
        
    def getAnimalName(self):
        return self.logger.getAnimalName() 

    def get_position(self):
        return(self.due.position[1])

    def get_nLaps(self):
        return(self.due.laps[1])

    def getLickTime(self):
        return self.due.licks[0]/1000.

    def getNLicks(self):
        return self.due.licks[1]
    
    def setAbsolutePosition(self):
        self.ordersQ.put(ABSOLUTE_POS)

    def setRelativePosition(self):
        self.ordersQ.put(RELATIVE_POS)

    def setRewardOnLickDistance(self,distance_ticks=-1):
        '''
        reward on lick after some distance (on the arduino)
        '''
        self.ordersQ.put(SET_REWARD_ON_LICK_DISTANCE + '{0}{1}'.format(SEP,int(distance_ticks)))
    
    def disableRewardOnLap(self):
        self.ordersQ.put(DISABLE_REWARD)    

    def enableRewardOnLap(self):
        self.ordersQ.put(ENABLE_REWARD)

    def getRigStatus(self):
        return dict(duetime = self.due.duinotime.value/1000.,
                    position = self.due.position/np.array([1000.,1]),
                    laps = self.due.laps[:]/np.array([1000.,1]),
                    licks = self.due.licks[:]/np.array([1000.,1]),
                    screen = self.due.screen[:]/np.array([1000.,1]),
                    imaging = self.due.imaging[:]/np.array([1000.,1]),
                    cam1 = self.due.cam1[:]/np.array([1000.,1]),
                    cam2 = self.due.cam2[:]/np.array([1000.,1]),
                    cam3 = self.due.cam3[:]/np.array([1000.,1]),
                    reward = self.due.reward[:]/np.array([1000.,1]),
                    actuator0 = self.due.actuator0[:]/np.array([1000.,1]),
                    actuator1 = self.due.actuator1[:]/np.array([1000.,1]))
    def startExperiment(self):
        if self.running_experiment.is_set():
            self.stopExperiment()
        if self.resetLog:
            self.logger.stoplog()
        print('Behavior rig is starting experiment.')
        self.due.start_experiment()
        if self.resetLog:
            self.logger.startlog()
        self.running_experiment.set()

    def stopExperiment(self):
        self.due.stop_experiment()
        time.sleep(2)
        self.logger.stoplog()
        self.running_experiment.clear()
    
    def close(self):
        self.due.close()
        self.logger.close()
        try:
            self.due.join()
            self.logger.join()
        except: 
            print('Rig was already closed.')
        
def main():
    port = '/dev/ttyACM0'
    baudrate = 115200
    rig = BehaviorRig(animalName = 'JC028', port = port)
    
    rig.start_experiment()
    # Run for half hour
    
    for i in range(180):
        print('Position: '+ str(rig.get_position())+ " - nLaps: "+str(rig.get_nLaps()))
        time.sleep(10)
        
    rig.stop_experiment()
    rig.close()
    sys.exit()
        

if __name__ == '__main__':
    main()
