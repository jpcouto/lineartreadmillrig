from PyQt5.QtWidgets import (QWidget,
                             QApplication,
                             QGridLayout,
                             QFormLayout,
                             QVBoxLayout,
                             QHBoxLayout,
                             QTabWidget,
                             QCheckBox,
                             QPushButton,
                             QInputDialog,
                             QTextEdit,
                             QLineEdit,
                             QSpinBox,
                             QSlider,
                             QComboBox,
                             QLabel,
                             QAction,
                             QProgressBar,
                             QMenuBar,
                             QGraphicsView,
                             QGraphicsScene,
                             QGraphicsItem,
                             QGraphicsLineItem,
                             QGroupBox,
                             QTableWidget,
                             QMainWindow,
                             QDockWidget,
                             QFileDialog,
                             QTreeView)
from PyQt5.QtGui import (QImage, QPixmap,QBrush,QPen,
                         QColor,
                         QStandardItemModel,
                         QStandardItem)
from PyQt5.QtCore import Qt,QSize,QRectF,QLineF,QPointF,QTimer
import pyqtgraph as pg
pg.setConfigOption('background',[200,200,200])
#pg.setConfigOption('crashWarning',True)
import time
from datetime import datetime
import numpy as np
import warnings

colors = ['#d62728',
          '#1f77b4',
          '#ff7f0e',
          '#2ca02c',
          '#9467bd',
          '#8c564b',
          '#e377c2',
          '#7f7f7f',
          '#bcbd22',
          '#d62728',
          '#1f77b4']

class RigWidget(QWidget):
    def __init__(self,rig=None,timer_period = 30,plots_timer_period = 30,
                 ignore_warnings = True):
        super(RigWidget,self).__init__()
        if ignore_warnings:
            warnings.filterwarnings('ignore')
        self.timerperiod = timer_period
        self.plots_timerperiod = plots_timer_period
        self.rig = rig
        self.tstart = time.time()
        self.timer = QTimer()
        self.plots_timer = QTimer()
        grid = QGridLayout()
        wid = QWidget()
        form = QFormLayout()
        self.rigStatus = QLabel(' ')
        grid.addWidget(self.rigStatus,1,0)
        self.posbuflen = 500
        self.buflen = 500
        if not rig is None:
            self.name = 'Rig {0}'.format(self.rig.port)
            self.win = pg.GraphicsLayoutWidget()
            self.p1 = self.win.addPlot()
            grid.addWidget(self.win,0,0,1,2)
            self.p1.getAxis('bottom').setPen('k')
            self.p1.getAxis('left').setPen('k')
            self.buf = dict(position = np.zeros((self.posbuflen,2)),
                            laps = np.zeros((self.buflen,2)),
                            licks = np.zeros((self.buflen,2)),
                            reward = np.zeros((self.buflen,2)),
                            screen = np.zeros((self.buflen,2)),
                            actuator0 = np.zeros((self.buflen,2)),
                            cam1 = np.zeros((self.buflen,2)),
                            cam2 = np.zeros((self.buflen,2)),
                            cam3 = np.zeros((self.buflen,2)),
                            imaging = np.zeros((self.buflen,2)))
            self.clear_buffer()
            self.plots = dict(
                position=pg.PlotCurveItem(pen = pg.mkPen(color='k',width = 2)),
                laps=pg.PlotCurveItem(connect = 'pairs',
                                      pen = pg.mkPen(color=colors[0],width = 3)),
                licks=pg.PlotCurveItem(connect = 'pairs',
                                       pen = pg.mkPen(color=colors[2],width = 3)),
                reward=pg.PlotCurveItem(connect = 'pairs',
                                        pen = pg.mkPen(color=colors[3],width = 3)),
                screen=pg.PlotCurveItem(connect = 'pairs',
                                        pen = pg.mkPen(color=colors[4],width = 3)),
                actuator0=pg.PlotCurveItem(connect = 'pairs',
                                           pen = pg.mkPen(color=colors[5],width = 3)),
                cam1=pg.PlotCurveItem(connect = 'pairs',
                                      pen = pg.mkPen(color=colors[6],width = 3)),
                cam2=pg.PlotCurveItem(connect = 'pairs',
                                      pen = pg.mkPen(color=colors[7],width = 3)),
                cam3=pg.PlotCurveItem(connect = 'pairs',
                                      pen = pg.mkPen(color=colors[8],width = 3)),
                imaging=pg.PlotCurveItem(connect = 'pairs',
                                         pen = pg.mkPen(color=colors[9],width = 3)))
            for p in self.plots.keys():
                self.p1.addItem(self.plots[p])
            rewardButton = QPushButton('Reward')
            rewardButton.clicked.connect(self.buttonReward)
            form.addRow(rewardButton)
            self.lapReset = QCheckBox()
            self.lapReset.setChecked(True)
            self.lapReset.stateChanged.connect(self.lapResetChanged)
            form.addRow(QLabel('Lap resets position:'),self.lapReset)
            self.lapReward = QCheckBox()
            self.lapReward.setChecked(True)
            self.lapReward.stateChanged.connect(self.rewardOnLapChanged)
            form.addRow(QLabel('Lap gives reward:'),self.lapReward)

            self.rewardOnLickDistance = QSpinBox()
            self.rewardOnLickDistance.setValue(0)
            self.rewardOnLickDistance.setRange(0,10000)
            self.rewardOnLickDistance.setSingleStep(100)
            
            def setRewardOnLickDistance():
                val = self.rewardOnLickDistance.value()
                if val == 0:
                    val = -1
                self.rig.setRewardOnLickDistance(val)
                if val > 0:
                    self.lapReward.setChecked(False)
                else:
                    self.lapReward.setChecked(True)
                
            self.rewardOnLickDistance.valueChanged.connect(setRewardOnLickDistance)
            form.addRow(QLabel('Reward on distance after lick:'),self.rewardOnLickDistance)            
            
            self.rewardSize = QSlider(Qt.Horizontal)
            self.rewardSize.setMaximum(200)
            self.rewardSize.setMinimum(5)
            self.rewardSize.setSingleStep(1)
            self.rewardSize.setValue(25) 
            self.rewardSize.valueChanged.connect(self.setRewardSize)
            form.addRow(QLabel('Reward size:'),self.rewardSize)
        
            actuator0Button = QPushButton('actuator0')
            actuator0Button.clicked.connect(self.buttonActuator0)
            actuator1Button = QPushButton('actuator1')
            actuator1Button.clicked.connect(self.buttonActuator1)
            form.addRow(actuator0Button,actuator1Button)
            self.actuatornumber = QLineEdit('0')
            self.actuatornpulses = QLineEdit('5')
            self.actuatorwidth = QLineEdit('10')
            self.actuatorperiod = QLineEdit('90')
            self.actuatorpositiontrigger = QLineEdit('-1')
            self.actuatornumber.returnPressed.connect(self.setActuatorParameters)
            self.actuatornpulses.returnPressed.connect(self.setActuatorParameters)
            self.actuatorwidth.returnPressed.connect(self.setActuatorParameters)
            self.actuatorpositiontrigger.returnPressed.connect(self.setActuatorParameters)
            form.addRow(QLabel('Actuator'),self.actuatornumber)
            form.addRow(QLabel('Actuator npulses'),self.actuatornpulses)
            form.addRow(QLabel('Actuator width'),self.actuatorwidth)
            form.addRow(QLabel('Actuator period'),self.actuatorperiod)
            form.addRow(QLabel('Actuator position trigger'),self.actuatorpositiontrigger)
            
            self.timer.timeout.connect(self.setStatus)
            self.timer.start(self.timerperiod)
            self.plots_timer.timeout.connect(self.update_plot)
            self.plots_timer.start(self.plots_timerperiod)
            def ptimerstate(value):
                if value:
                    self.clear_buffer()
                    self.start_plots()
                else:
                    self.stop_plots()

            self.ptimer = QCheckBox()
            self.ptimer.setChecked(True)
            self.ptimer.stateChanged.connect(ptimerstate)
            form.addRow(QLabel('Plot rig (dont use during experiments):'),self.ptimer)
            def pserverstate(value):
                if value:
                    if not hasattr(self,'zmqserver'):
                        from .server.interface import RigZMQServer
                        self.zmqserver = RigZMQServer(self.rig)
                    if self.zmqserver.is_stopped.is_set():
                        self.zmqserver.is_stopped.clear()
                    else:
                        print('Server was running so did nothing.')
                else:
                    if hasattr(self,'zmqserver'):
                        self.zmqserver.is_stopped.set()
                        
            self.pserver = QCheckBox()
            self.pserver.setChecked(False)
            self.pserver.stateChanged.connect(pserverstate)
            form.addRow(QLabel('zmq server:'),self.pserver)
        else:
            self.name = 'Not connected to any rig.'
        wid.setLayout(form)
        grid.addWidget(wid,1,1)
        self.setLayout(grid)
        self.setWindowTitle('Treadmill '+self.name)

    def start_plots(self):
        self.ptimer.setChecked(True)
        for p in self.plots.items():
            p[1].show()
            self.plots_timer.start(self.plots_timerperiod)

    def stop_plots(self):
        self.ptimer.setChecked(False)
        self.plots_timer.stop()
        for p in self.plots.items():
            p[1].hide()


    def clear_buffer(self):
        for n in self.buf.keys():
            self.buf[n][:] = np.nan
            
    def display(self,msg):
        print('[RigWidget - '+datetime.today().strftime('%y-%m-%d %H:%M:%S')+'] - ' + msg)

    def setActuatorParameters(self):
        try:
            actuator = int(self.actuatornumber.text())
            npulses = int(self.actuatornpulses.text())
            width = int(self.actuatorwidth.text())
            period = int(self.actuatorperiod.text())
            position_trigger = int(self.actuatorpositiontrigger.text())
        except:
            print('Could not interpret parameters.')
        finally:
            self.rig.setActuatorParameters(actuator,npulses,width,period,position_trigger)
    def setStatus(self):
        if self.rig is None:
            self.rigStatus.setText('''Time : {0:.2f} s'''.format(time.time()-self.tstart))
        else:
            s = self.rig.getRigStatus()
            self.rigStatus.setText('''Time : {0:.2f} s
due time: {1:.2f} 
Last lick [{2}]: {3:.2f} 
Last lap [{4}]: {5:.2f}
VStim Pulses: {6}
Imaging pulses: {7}
cam1 pulses: {8}
cam2 pulses: {9}
cam3 pulses: {10}
Lap position: {11} '''.format(time.time()-self.tstart,s['duetime'], 
                             s['licks'][1],
                              s['licks'][0],
                              s['laps'][1],
                              s['laps'][0],
                              s['screen'][1],
                              s['imaging'][1],
                              s['cam1'][1],
                              s['cam2'][1],
                              s['cam3'][1],
                              s['position'][1]))
            if self.plots_timer.isActive():
                for k in self.buf.keys():
                    if not self.buf[k][-1,0] == s[k][0]:
                        self.buf[k] = np.roll(self.buf[k],-1,axis = 0)
                        self.buf[k][-1] = s[k]
    def lapResetChanged(self,state):
        #self.rig.setLapReset(state)
        if not self.rig is None:
            if state:
                self.display('Rig: lap will reset position.')
                self.rig.setRelativePosition()
            else:
                self.display('Rig: lap will not reset position.')            
                self.rig.setAbsolutePosition()
            
    def rewardOnLapChanged(self,state):
        if not self.rig is None:
            if state:
                self.display('Rig: lap will give reward.')
                self.rig.enableRewardOnLap()
            else:
                self.display('Rig: lap will not give reward.')            
                self.rig.disableRewardOnLap()

    def setRewardSize(self,value):
        rsize = int(value)
        if not self.rig is None:
            self.display('Rig: setting reward size {0}'.format(rsize))
            self.rig.setRewardSize(rsize)
                    
    def buttonReward(self):
        if not self.rig is None:
            self.display('Rig: giving reward.')
            self.rig.giveReward()

    def buttonActuator0(self):
        if not self.rig is None:
            self.display('Rig: triggering actuator0.')
            self.rig.activateActuator0()
    def buttonActuator1(self):
        if not self.rig is None:
            self.display('Rig: triggering actuator1.')
            self.rig.activateActuator1()
    def buttonTrigger(self):
        if not self.rig is None:
            self.display('Rig: triggering start.')
            self.rig.startExperiment()
    def update_plot(self):
        with warnings.catch_warnings():
            warnings.filterwarnings('ignore')

            self.plots['position'].setData(x = self.buf['position'][:,0],
                                           y = self.buf['position'][:,1])

            size = 100
            plts = ['screen','laps','licks','reward','actuator0','cam1','cam2','cam3','imaging']
            for offset,k in zip(np.arange(len(plts))*(size+20),plts):
                l = self.buf[k][:,0]
                l = l[(~np.isnan(l)) & (l>np.nanmin(self.buf['position'][:,0]))]
                #if len(l):
                l = np.sort(np.repeat(l,2))
                y = np.zeros_like(l)-offset
                y[::2] -= -1*size
                self.plots[k].setData(x = l,y = y)


def main():
    import argparse
    import sys
    import os
    parser = argparse.ArgumentParser(
        description='Connect to an arduino linear treadmill rig (the zmq interface)')
    parser.add_argument('-p','--port', action='store',default=0,type=int)
    args = parser.parse_args(sys.argv[1:])
    print('Running due zmq controller')
    from sys import platform
    if platform == "linux" or platform == "linux2":
        # linux
        port = '/dev/ttyACM'
    elif platform == "darwin":
        # OS X
        port = '/dev/tty.usbmodemfd'
    elif platform == "win32":
        port = 'COM'
    portNumber = args.port
    print('Connecting to rig on port {0}{1}'.format(port,portNumber))
    from .rig import BehaviorRig
    rig = BehaviorRig(port = '{0}{1}'.format(port,portNumber),log = False)
    #### APP ####
    app = QApplication(sys.argv)
    w = RigWidget(rig = rig)
    w.show()
    sys.exit(app.exec_())
    
