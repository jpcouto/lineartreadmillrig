
import time
import zmq
from psychopy import visual,data,event,core,parallel,logging
from psychopy.visual.windowwarp import Warper
from datetime import datetime
from rig import BehaviorRig
import sys
import numpy as np
import time
def main():
    def display(msg):
        print('['+datetime.today().strftime('%y-%m-%d %H:%M:%S')+'] - ' + msg)
    port = 'COM15'
    animalName='JC105'
    rig = BehaviorRig(animalName = animalName, port = port)
    rig.disableRewardOnLap()
    ntrials = 4
    nori = 4
    orientations = np.linspace(0,360,nori)
    gain = -0.00001
    FULLSCRN = True
    win = visual.Window([2048,1536],rgb=(0,0,0),units='norm',
                        screen=0,fullscr = FULLSCRN)
    #warper = Warper(win,
    #                warp='spherical',
    #                warpfile = "",
    #                warpGridsize = 128,
    #                eyepoint = [0.5, 0.5],
    #                flipHorizontal = False,
    #                flipVertical = False)
    #warper.dist_cm = 10
    grat = visual.GratingStim(win,tex='sqr',
                              sf=2,
                              mask=None,
                              name='grating',
                              ori=180,
                              contrast=.8,
                              size=1.4,
                              pos=[0,0],units='norm')
    circles = []
    ncircles = 150
    xpos = np.random.uniform(-30,5,ncircles)
    ypos = np.random.uniform(-1,1,ncircles)
    rad = np.random.uniform(0.01,0.2,ncircles)
    col = np.clip(np.random.uniform(-10,10,ncircles),-1,1)
    for p in range(ncircles):
        ps = np.array([xpos[p],ypos[p]])
        c = visual.Circle(win,radius=rad[p],units = 'norm',
                          fillColor = np.array([1.,1.,1.])*float(col[p]),
                          pos = ps)
        circles.append([ps,c])
        postext = visual.TextStim(win,text='Displacement = 0.0',
                                  pos=(-1,-1),
                                  color = [1,0,0],
                                  contrast=1,
                                  height = 0.05,
                                  alignHoriz = 'left',
                                  alignVert = 'bottom',
                                  bold = True)

    running = True
    nframes = 0
    previous = 0
    lap_length = 75
    global tLastReward,tstart,tframe,rewardGiven,nrewards,rewardPos,pos
    nrewards = 0
    tLastReward = 0
    tstart = time.time()
    tframe = 0
    rewardGiven = False
    rewardPos = 0
    pos = 0
    tend = None
    def assessReward(licked, interval = 3):
        global tLastReward,tstart,tframe,rewardGiven,nrewards,rewardPos,pos
        ##### CONDITIONS ########
        if ((tframe - tLastReward > interval) and
            np.abs(rewardPos - pos) > 20 and
            not rewardGiven and licked):
            rewardGiven = True
            rewardPos  = pos
            nrewards += 1
            rig.giveReward()
            tLastReward = tframe
            #elif (tframe - tLastReward > interval):
            #    rewardGiven = False
        
    lastLick = -1
    duedat = rig.getRigStatus()
    lickCounter = duedat['licks'][1]
    logging = False
    duration = 60*60 
    while running:
        tframe = time.time()-tstart
        duedat = rig.getRigStatus()
        pos = duedat['position'][1]
        if lickCounter < duedat['licks'][1]:
            lickCounter = duedat['licks'][1]
            lastLick = tframe
        increment = pos-previous
        keyb = event.getKeys(['left','right','q','p'])
        if 'right' in keyb:
            grat.phase -= (1./(300*30))
        elif 'left' in keyb:
            grat.phase += (1./(300*30))
        elif 'q' in keyb:
            rig.stopExperiment()
            running = False
        elif 'p' in keyb:
            if not logging:
                rig.startExperiment()
                logging = True
                tframe = 0
                tend = duration
                tstart = time.time()
                tlastReward = tframe
                nrewards = 0
                rewardPos = 0
                rewardGiven = False
        for (p,c) in circles:
            pp = p + np.array([1,0])*pos/250.
            x,y = pp
            if np.abs(x) <= 1.2 and np.abs(y) <= 1.2:
                c.setPos(pp)
                c.draw()
        if pos > 0 and pos < 1000:
            grat.phase +=(increment)*gain
            grat.contrast = 1
            grat.draw()    
            if lastLick == tframe:
                assessReward(True,interval = 3)
            
            else:
                assessReward(False,interval = 3)
        else:
            rewardGiven = False
        postext.text = '''Rewards = {4}
        Laps = {3}
        Licks = {2}
        Displacement = {0}
        Time = {1:.1f}'''.format(pos,tframe,duedat['licks'][1],
                                 duedat['laps'][1],
                                 nrewards)
        if rig.running_experiment.is_set():
            postext.color = [1,0,0]
        else:
            postext.color = [1,1,0]
        postext.draw()
        win.flip()
        previous = pos

        nframes += 1
        if not tend is None:
            if tend < tframe:
                rig.stopExperiment()
                tend = None
    rig.close()

if __name__ == '__main__':
    main()
