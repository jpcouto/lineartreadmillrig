from bokeh.layouts import column,row,gridplot
from bokeh.models import (ColumnDataSource,
                          Span,
                          HoverTool, TapTool,
                          DatetimeTickFormatter,
                          Range1d,
                          Legend,
                          DataTable, DateFormatter, TableColumn)
from bokeh.models.widgets import (Toggle,
                                  TextInput,
                                  Select,
                                  Paragraph,
                                  Button,
                                  Dropdown)
import bokeh.palettes 
from bokeh.plotting import figure, curdoc,show
from bokeh.models.renderers import GlyphRenderer


import numpy as np
from rig import (parseBehaviorLog,
                 velocityFromPosition,
                 ticksToPosition,
                 listTrainingSessions,
                 getSessionsSummary)
from glob import glob
from os.path import join as pjoin
import os
from datetime import datetime, timedelta
from collections import OrderedDict
from scipy.signal import medfilt

animals,sessions = listTrainingSessions()
idx = np.argmax([np.max(sessions[k]['dates']) for k in animals])
curanimal = animals[idx]

# For the overview
ndayspast = 30
oneweekago = (datetime.today() - timedelta(days=ndayspast)).date()
daysfromLastWeek = np.array([(datetime.today() - timedelta(days=d)).date()
                             for d in range(ndayspast,-1,-1)])


lastweekdata = dict(animal=[],
                    date=[],
                    nlaps=[],
                    duration=[],
                    lapsPer30min=[],
                   color = [])

cidx = [3,10,15,30,50,70]
colors = bokeh.palettes.Spectral[len(cidx)]
for i,k in enumerate(animals):
    dates = np.array([d.date() for d in sessions[k]['dates']])
    tmpidx = np.where(dates>=oneweekago)[0]
    if len(tmpidx):
        tmpsummary = getSessionsSummary([k],sessions)[0]#.iloc[tmpidx]
        sesdays = np.array([d.date() for d in tmpsummary.date])
        for j,day in enumerate(daysfromLastWeek):
            p = np.where(sesdays == day)[0]
            lastweekdata['animal'].append(k)
            lastweekdata['date'].append(day.strftime("%d/%m/%y"))
            lastweekdata['color'].append("white")
            if len(p):
                p = p[-1]
                lastweekdata['lapsPer30min'].append(30.*(
                    tmpsummary.iloc[p].nLaps/
                    float(tmpsummary.iloc[p].duration/60.)))
                lastweekdata['nlaps'].append(tmpsummary.iloc[p].nLaps)
                lastweekdata['duration'].append(tmpsummary.iloc[p].duration)
                lastweekdata['color'][-1] = 'gray'
                for m,c in enumerate(cidx):
                    if lastweekdata['lapsPer30min'][-1] >= c:
                        lastweekdata['color'][-1] = colors[m]
                
            else:
                lastweekdata['nlaps'].append(0)
                lastweekdata['duration'].append(0)
                lastweekdata['lapsPer30min'].append(0)

# must give a vector of image data for image parameter
lastweekssource = ColumnDataSource(data=lastweekdata)

# Data for figures
summarysource = ColumnDataSource(dict(date = [],
                                      date_str = [],
                                      duration = [],
                                      trunning = [],
                                      tback = [],
                                      nLapsPerMin=[],
                                      nLaps = [],
                                      velRun=[],
                                      velMean=[]))
                                
sessionsource = ColumnDataSource(dict(time = [],
                                      lapposition = [],
                                      velocity = []))

sessionsource_lap = ColumnDataSource(dict(lapx = [],
                                          lapy = []))

sessionsource_reward = ColumnDataSource(dict(
                                          rewx = [],
                                          rewy = []))
sessionsource_lick = ColumnDataSource(dict(
                                          lickx = [],
                                          licky = []))

columns = [
        TableColumn(field="date", title="Session",
                    formatter=DateFormatter()),
        TableColumn(field="duration", title="Duration"),
        TableColumn(field="nLaps", title="Laps"),
]

# The summary plot
TOOLS = "hover,tap,save,reset"
f0 = figure(title="Training last month",
           x_range = [day.strftime("%d/%m/%y") for day in daysfromLastWeek],
           y_range=[a for a in sorted(set(lastweekdata['animal']))],
           x_axis_location="above", plot_width=1000, plot_height=700,
          tools=TOOLS)
f0.rect("date", "animal", 1, 1, source=lastweekssource,
    color="color", line_color=None)
f0.grid.grid_line_color = None
f0.axis.axis_line_color = None
f0.axis.major_tick_line_color = None
f0.axis.major_label_text_font_size = "12pt"
f0.axis.major_label_standoff = 0
f0.xaxis.major_label_orientation = np.pi/3

hover = f0.select(dict(type=HoverTool))
hover.tooltips = OrderedDict([
    ('animal','@animal'),
    ('date', '@date'),
    ('nlaps', '@nlaps'),
    ('duration', '@duration'),
    ('laps in half hour', '@lapsPer30min')
])

TOOLS="pan,wheel_zoom,box_zoom,reset,hover,tap,previewsave"
f1 = figure(tools=TOOLS,
            title="Experiment duration", width=700, height=200,
            x_axis_type="datetime",
            background_fill_color = "beige",
            background_fill_alpha = 0.5)
f1.yaxis.axis_label = 'Time (minutes)'
# duration of experiment
scatters = []
scatters.append(f1.scatter(x='date',y = 'duration',source=summarysource))
f1.line(x='date',y = 'duration',source=summarysource)
# time running
scatters.append(f1.scatter(x='date',y = 'trunning',source=summarysource,
                           color='orange'))
f1.line(x='date',y = 'trunning',source=summarysource,color='orange')
# time backward
scatters.append(f1.scatter(x='date',y = 'tback',
                           source=summarysource,color='red'))
f1.line(x='date',y = 'tback',
        source=summarysource,color='red')
legend = Legend(items=[
    ("Training duration"   , [scatters[0]]),
    ("Time running" , [scatters[1]]),
    ("Time backwards" , [scatters[2]])],
                location=(0, 0),
                glyph_height=10,glyph_width=10)
f1.add_layout(legend, 'right')


f2 = figure(tools=TOOLS,
            title="Laps per min", width=700, height=200,
            x_range=f1.x_range,x_axis_type="datetime",
            background_fill_color = "beige",
            background_fill_alpha = 0.5)

f2.y_range = Range1d(0,5)
f2.yaxis.axis_label = '# laps/min'
# number of laps per minute
scatters.append(f2.scatter(x='date',y = 'nLapsPerMin',source=summarysource))
p = f2.line(x='date',y = 'nLapsPerMin',source=summarysource)

f3 = figure(tools=TOOLS,
            title="Velocity", width=700, height=200,
            x_range=f1.x_range,x_axis_type="datetime",
            background_fill_color = "beige",
            background_fill_alpha = 0.5)

f3.y_range = Range1d(0,30)
f3.xaxis.axis_label = 'Day of training [m/d]'
f3.yaxis.axis_label = 'cm/s'

# Velocity while running
scatters.append(f3.scatter(x='date',y = 'velRun',source=summarysource))
f3.line(x='date',y = 'velRun',source=summarysource)
# Mean velocity
scatters.append(f3.scatter(x='date',y = 'velMean',source=summarysource,color='orange'))
f3.line(x='date',y = 'velMean',source=summarysource,color='orange')


p = gridplot([[f1], [f2], [f3]])
TOOLS="pan,xbox_zoom,wheel_zoom,reset,previewsave"

f4 = figure(tools=TOOLS,
            title="Animal velocity", width=900, height=200)
f4.line(x='time',y = 'velocity',source=sessionsource)
f4.yaxis.axis_label = 'velocity [cm/s]'
f5 = figure(tools=TOOLS,
            title="Lap position ", width=900, height=200,
            x_range = f4.x_range)
f5.line(x='time',y = 'lapposition',source=sessionsource)
f5.xaxis.axis_label = 'time [s]'
f5.yaxis.axis_label = 'position [cm]'
f5.ray(x='lapx',y='lapy',source=sessionsource_lap,
       angle=90,angle_units="deg",length=5,color='orange',line_width=2)
f5.ray(x='rewx',y='rewy',source=sessionsource_reward,
       angle=90,angle_units="deg",length=5,color='green',line_width=2)
f5.ray(x='lickx',y='licky',source=sessionsource_lick,
       angle=90,angle_units="deg",length=5,color='red',line_width=2)

selmenu = []
for a in animals:
    selmenu.append((a,a))
seldropdown = Dropdown(label="Select animal", menu=selmenu)

data_table = DataTable(source=summarysource, columns=columns, width=400, height=280)


def sessionSelect(attr,old,new):
    global curanimal
    val = new['1d']['indices']
    if len(val) < 1:
        return
    selected = val
    dffactor = 20
    t = []
    v = []
    p = []
    if len(selected)>1:
        print('Selection of multiple traces not implemented yet..')
    for val in selected:
        ses = sessions[curanimal]['paths'][int(val)]
        print(ses)
        posticks,lap, reward,lick,screenPulses,comments = parseBehaviorLog(ses,trimInconsistent=False)
        try:
            time,position = ticksToPosition(posticks[:,1],posticks[:,2])
        except:
            print('Is this log empty?? ' + ses)
            return
        velocity = medfilt(velocityFromPosition(time,position),11)
        t.append(time[::dffactor])
        v.append(velocity[::dffactor])
        p.append(np.mod(position,150)[::dffactor])
    if lap.shape[1] > 1:
        ndata = {'lapx':lap[:,1],
                 'lapy': np.ones_like(lap[:,1])*150}
        sessionsource_lap.data.update(**ndata)
        ndata = {'rewx':reward[:,1],
                 'rewy': np.ones_like(reward[:,1])*155}
        sessionsource_reward.data.update(**ndata)
    if len(lick) == 0:        
        sessionsource_lick.data.update(**{'lickx':[],
                                          'licky': []})
    else:
        sessionsource_lick.data.update(**{'lickx':lick[:,1],
                                          'licky': np.ones_like(lick[:,1])*165})
    ndata = {'time':t[0],
             'lapposition':p[0],
             'velocity':v[0]}
    sessionsource.data.update(**ndata)
    f4.title.text = os.path.basename(ses)+ ' animal velocity'

def animalselect_handler(value):
    print("Getting data for animal: %s" % value)
    global curanimal
    curanimal = value
    print('Animal is set for: ' + curanimal)
    animals,sessions = listTrainingSessions() # Update file list...
    summary = getSessionsSummary([value],sessions)
    ndata = {'date': summary[0]['date'],
             'date_str':[d.strftime("%b %d [%A] %H:%M") for d in summary[0]['date']],
             'duration': np.round(summary[0]['duration']/60.),
             'trunning': (summary[0]['timeRunning'])/60.,
             'tback': (summary[0]['timeBackward'])/60.,
             'nLapsPerMin': 60.*(summary[0]['nLaps']/summary[0]['duration']),
             'nLaps': summary[0]['nLaps'],
             'velRun': summary[0]['velocityMean'],
             'velMean': summary[0]['velocityRunning']}
    summarysource.data.update(**ndata)
    f1.title.text = curanimal +' time on training'
    # Update the other menu
    sessionSelect([],[],{'1d':{'indices':[len(sessions[curanimal]['dates'])-1]}})

animalselect_handler(animals[idx])
seldropdown.on_click(animalselect_handler)

def rectselected(attr,old,new):
    value = lastweekssource.data['animal'][new[0]]
    print("Getting data for animal: %s" % value)
    global curanimal
    curanimal = value
    print('Animal is set for: ' + curanimal)
    animals,sessions = listTrainingSessions() # Update file list...
    summary = getSessionsSummary([value],sessions)
    ndata = {'date': summary[0]['date'],
             'date_str':[d.strftime("%b %d [%A] %H:%M") for d in summary[0]['date']],
             'duration': np.round(summary[0]['duration']/60.),
             'trunning': (summary[0]['timeRunning'])/60.,
             'tback': (summary[0]['timeBackward'])/60.,
             'nLapsPerMin': 60.*(summary[0]['nLaps']/summary[0]['duration']),
             'nLaps': summary[0]['nLaps'],
             'velRun': summary[0]['velocityMean'],
             'velMean': summary[0]['velocityRunning']}
    summarysource.data.update(**ndata)
    f1.title.text = curanimal +' time on training'
    # Update the other menu
    sessionSelect([],[],{'1d':{'indices':[len(sessions[curanimal]['dates'])-1]}})
    return None
lastweekssource.selected.on_change('indices',rectselected)

summarysource.on_change('selected', sessionSelect)
tool = []
for f in [f1,f2,f3]:
    tool.append(f.select(dict(type=HoverTool)))
    tool[-1].tooltips=[
        ("session", "@index"),
        ("(date,y)", "(@date_str, $y)"),
        ("duration","@duration"),
        ("laps","@nLaps")]
    tool[-1].mode  = 'mouse'
    tool.append(f.select(dict(type=TapTool)))

curdoc().add_root(column(f0,seldropdown,p,data_table,gridplot([[f4],[f5]])))
curdoc().title = 'Training summary data'
