from bokeh.layouts import column,row,gridplot,layout
from bokeh.models import ColumnDataSource
from bokeh.models.widgets import Toggle,TextInput,Select,Paragraph,Button,Div,Slider,RadioGroup
from bokeh.palettes import RdYlBu3
from bokeh.plotting import figure, curdoc
import time
import zmq
from datetime import datetime
import sys
from functools import partial

def display(msg):
    print('['+datetime.today().strftime('%y-%m-%d %H:%M:%S')+'] - ' + msg)

def getRigStatus(rig):
    socket.send_pyobj({'action':'info','rig':rig})
    return socket.recv_pyobj()

tasksMenu = ["TaskRewardOnLap",
             "TaskLickOnDistance"]


rignumber = 0
portnumber = 0

portnumber = 0
for cmd in sys.argv:
    if '--port' in cmd:
        ports = cmd.split('=')[1]
        portnumber = [int(p) for p in ports.split(',')]
socketnumber = '10000{0}'.format(portnumber[0]) 
context = zmq.Context()
socket = context.socket(zmq.REQ)
socket.connect('tcp://localhost:{}'.format(socketnumber))

nrigs = len(portnumber)
srate = 30
durplot = 10
default_reward_size = 25
npoints = int(durplot*(1./(srate/1000.))) # 10 seconds of points
rig = [getRigStatus(p) for p in range(nrigs)]
display('Read status from zmq controller.')
lastStat = rig

def rigstatus(r):
    if not r['experimentRunning']:
        r['time'] = 0
    return '''
Time: {time:.0f} \n
Animal name: {animalName} \n
Lap position: {position} \n
Number of laps: {nlaps} \n
Last reward time: {lastReward} \n
Last lick time: {lastLick} \n
'''.format(**r)
status_text = []
source = []
source_reward = []
source_lick = []

def startExperiment(r):
    
    stat = getRigStatus(r)
    if not stat['experimentRunning']:
        source[r].data = dict(time=[],position=[])
        source_reward[r].data = dict(x=[],y=[])
        source_lick[r].data = dict(x=[],y=[])
        display('Ordering start of experiment [' + animalName[r].value + ' - ' +
                duration[r].value + '].')
        socket.send_pyobj({'rig':r,
                           'action':'name',
                           'value':animalName[r].value})
        msg = socket.recv_pyobj()
        display('Animal name set.')
        socket.send_pyobj({'rig':r,
                           'action':'start',
                           'value':float(duration[r].value)*60})
        msg = socket.recv_pyobj()
        display('Start command sent.')
        sendWeight(None,None,None,r)
    else:
        display('Experiment is already running...')

def stopExperiment(r):
    stat = getRigStatus(r)
    if stat['experimentRunning']:
        socket.send_pyobj({'rig':r,
                           'action':'stop'})
        msg = socket.recv_pyobj()
        display('Stopped experiment')
    else:
        display('Experiment is not running...')

def sendWeight(attr,old,new,r):
    tmp = animalWeight[r].value
    if len(tmp):
        try:
            tmp = float(tmp)
        except:
            print('Could not convert to float [{0}]'.format(tmp))
    if not type(tmp) is float:
        return
    stat = getRigStatus(r)
    if stat['experimentRunning']:  
            socket.send_pyobj({'rig':r,
                               'action':'logweight',
                               'value':animalWeight[r].value})
            msg = socket.recv_pyobj()
            display('Sending animal weight')
    else:
        display('Experiment is not running...')

def setTask(attr,old,new,r):
    tmp = tasksel[r].active
    stat = getRigStatus(r)
    if not stat['experimentRunning']:  
            socket.send_pyobj({'rig':r,
                               'action':'task',
                               'value':'|'.join([tasksMenu[tmp],
                                                 taskvalue[r].value])})
            msg = socket.recv_pyobj()
            display('Setting task')
    else:
        display('Experiment is running; stop it first.')

def setRewardSize(attr,old,new,r):
    tmp = reward_size[r].value
    socket.send_pyobj({'rig':r,
                       'action':'rewardsize',
                       'value':int(tmp)})
    msg = socket.recv_pyobj()

title_text = []
run_button = []
animalName = []
animalWeight = []
duration = []
stop_button = []
tasksel = []
taskvalue = []
p = []
posplot = []
rewardplot = []
lickplot = []
reward_size = []
startfunct = [partial(startExperiment,i) for i in range(nrigs)]
stopfunct = [partial(stopExperiment,i) for i in range(nrigs)]
sendWeightfunct = [partial(sendWeight,r=i) for i in range(nrigs)]
rewardsizefunct = [partial(setRewardSize,r=i) for i in range(nrigs)]
selectTaskfunct = [partial(setTask,r=i) for i in range(nrigs)]
lays = []
for i in range(nrigs):
    
    title_text.append(Div(text = '<h2>Rig {0}</h2>'.format(i)))
    
    status_text.append(Paragraph(text = rigstatus(getRigStatus(i))))
    source.append(ColumnDataSource(dict(time = [],
                                        position = [])))
    source_reward.append(ColumnDataSource(dict(x = [],
                                      y = [])))
    source_lick.append(ColumnDataSource(dict(x = [],
                                             y = [])))

        
    # The things that you can to control:
    run_button.append(Button(label="Start."))
    animalName.append(TextInput(value=rig[i]['animalName'],
                                title="Animal:"))

    tasksel.append(RadioGroup(labels=tasksMenu, active=0))
    taskvalue.append(TextInput(value='200',
                               title="Var:"))

    animalWeight.append(TextInput(value="",title="Weight:"))    
    duration.append(TextInput(value="60",title="Duration:"))
    reward_size.append(Slider(start=15, end=300, value=default_reward_size,
                              step=2,
                              title="Reward size:"))
    stop_button.append(Button(label="Stop"))
    
    run_button[i].on_click(startfunct[i])
    stop_button[i].on_click(stopfunct[i])
    reward_size[i].on_change('value',rewardsizefunct[i])
    animalWeight[i].on_change('value',sendWeightfunct[i])
    tasksel[i].on_change('active',selectTaskfunct[i])


    # The plot stuff
    p.append(figure(plot_height = 150,plot_width = 500,
                    background_fill_color = "beige",
                    background_fill_alpha = 0.5,
                    tools="xpan,xwheel_zoom,xbox_zoom,reset"))
    p[i].yaxis.axis_label = 'Lap position (# ticks)'
    p[i].xaxis.axis_label = 'Time (s)'
    p[i].x_range.follow = 'end'
    p[i].x_range.follow_interval = durplot
    p[i].x_range.range_padding = 0

    posplot.append(p[i].line(x='time',y = 'position',
                             alpha = 1, line_width = 2,
                             source = source[i]))
    rewardplot.append(p[i].ray(x='x',y='y',source=source_reward[i],
                               angle=90,angle_units="deg",length=1,
                               color='green',line_width=2))
    lickplot.append(p[i].ray(x='x',y='y',source=source_lick[i],
                     angle=90,angle_units="deg",length=1,
                     color='red',line_width=2))
    # Add them to the doc
    lays.append(layout([[title_text[i]],
                        [column(run_button[i],
                                stop_button[i],
                                animalName[i]),
                         column(animalWeight[i],
                                duration[i],
                                reward_size[i]),
                         column(row(tasksel[i],taskvalue[i]),
                                status_text[i])],
                        [p[i]]]))
col_lays = []
for l in range(0,len(lays),2):
    if l+1 == len(lays):
        col_lays.append(row(lays[l]))
    else:
        col_lays.append(row(lays[l],lays[l+1]))
    print(l)
    
curdoc().add_root(layout(col_lays))

global update_counter
update_counter = 0
from numpy import mod
# put the button and plot in a layout and add to the document
def update():
    global update_counter
    ii = mod(update_counter,nrigs)
    #    for ii in range(nrigs):
    stat = getRigStatus(ii)
    ri = stat['rig']
    data = dict(time=[stat['time']],position=[stat['position']])
    source[ii].stream(data,npoints)
    status_text[ii].text = rigstatus(stat)
    if not lastStat[ii]['lastReward'] == stat['lastReward']:
        rewarddata = dict(x=[stat['lastReward']],y=[0])
        source_reward[ii].stream(rewarddata, npoints)
    if not lastStat[ii]['lastLick'] == stat['lastLick']:
        lickdata = dict(x=[stat['lastLick']],y=[10])
        source_lick[ii].stream(lickdata, npoints)
    lastStat[ii] = stat
    update_counter += 1
curdoc().add_periodic_callback(update,srate)
curdoc().title = 'Linear treadmill rigs training'

