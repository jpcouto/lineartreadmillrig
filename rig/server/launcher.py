from .zmq_controller import zmq_rig_controller
import argparse
import sys
import os
import subprocess
import shlex
import platform

hostname = platform.node()

class CLIParser(object):
    def __init__(self):
        parser = argparse.ArgumentParser(
            description='Linear treadmill rig server-controller launcher',
            usage='''treadmill-rig <command> [<args>]

The commands are:
    due        Connect to an arduino linear treadmill rig (the zmq interface)
    remote     Launches the remote control gui
    summary    Launches the browser data explorer
''')
        parser.add_argument('command', help='Subcommand to run')
        # parse_args defaults to [1:] for args, but you need to
        # exclude the rest of the args too, or validation will fail
        args = parser.parse_args(sys.argv[1:2])
        if not hasattr(self, args.command):
            print 'Unrecognized command'
            parser.print_help()
            exit(1)
        print('Gonna try')
        # use dispatch pattern to invoke method with same name
        getattr(self, args.command)()
    
    def due(self):
        parser = argparse.ArgumentParser(
            description='Connect to an arduino linear treadmill rig (the zmq interface)')
        parser.add_argument('-p','--port', action='store',default=0,type=int,nargs='+')
        args = parser.parse_args(sys.argv[2:])
        print('Running due zmq controller')
        from sys import platform
        if platform == "linux" or platform == "linux2":
            # linux
            port = '/dev/ttyACM'
        elif platform == "darwin":
            # OS X
            port = '/dev/tty.usbmodemfd'
        elif platform == "win32":
            port = 'COM'
        portNumber = [p for p in args.port]
        print('\n'.join(['Connecting to rig on port {0}{1}'.format(port,p) for p in portNumber]))
        zmq_rig_controller(portnumber=portNumber, port = port)
    def remote(self):
        parser = argparse.ArgumentParser(
            description='Launches the remote control gui (bokeh app) that talks to the zmq server.')
        
        parser.add_argument('-p','--port', action='store',default=0,type=int,nargs='+')
        parser.add_argument('-b','--browser', action='store_true',default=False)
    
        args = parser.parse_args(sys.argv[2:])
        cmd = 'bokeh serve'
        if args.browser:
            cmd += ' --show'
        cmdpath = os.path.normpath(os.path.join(os.path.abspath(
            os.path.dirname(__file__)),'remote.py'))
        cmd += ' ' + cmdpath.encode('string-escape')
        #cmd += ' --host="*"'.encode('string-escape')
        cmd += ' --allow-websocket-origin="*"'#{0}:{1}'.format(hostname,5006)
        cmd += ' --args --port={0}'.format(','.join([str(p) for p in args.port]))
        # TO DO: PARSE THE ARGUMENTS TO THE BOKEH APP
        print('Running the remote server (bokeh app)')
        print('    ' + cmd)        
        p = subprocess.call(shlex.split(cmd), shell=False)

    def summary(self):
        parser = argparse.ArgumentParser(
            description='Launches the browser data explorer')
        parser.add_argument('-b','--browser', action='store_true',default=False)
        
        args = parser.parse_args(sys.argv[2:])
        cmd = 'bokeh serve'
        if args.browser:
            cmd += ' --show'
        cmdpath = os.path.normpath(os.path.join(os.path.abspath(
            os.path.dirname(__file__)),'summary.py'))
        cmd += ' ' + cmdpath.encode('string-escape')
#        cmd += ' --host="*"'
        cmd += ' --allow-websocket-origin="*"'#{0}:{1}'.format(hostname,5005)
        cmd += ' --port=5005'
        # TO DO: PARSE THE ARGUMENTS TO THE BOKEH APP
        print('Running the data explorer (bokeh app)')
        print('    '+cmd)
        p = subprocess.call(shlex.split(cmd), shell=False)

def main():
    CLIParser()

if __name__ == '__main__':
    main()

