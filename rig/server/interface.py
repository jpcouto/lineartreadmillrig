import time
import zmq
from datetime import datetime
from multiprocessing import Process,Event,Array
from ctypes import c_longdouble
import numpy as np
from ..rig import DueInterface,BehaviorRig

class RigZMQServer(Process):
    # Class to talk to a rig using a separate process through zmq.
    def __init__(self,rig,portnumber = 190000, timeout = 0.03, nuserfields = 20, autostart = True):
        Process.__init__(self)

        if isinstance(rig,BehaviorRig):
            rig = rig.due

        self.exit = Event()
        self.is_stopped = Event()
        
        self.experiment_running = Event()        
        self.expStartTime   = rig.expStartTime
        self.duinotime   = rig.duinotime
        self.position = rig.position
        self.laps = rig.laps
        self.licks = rig.licks
        self.reward = rig.reward
        self.cam1 = rig.cam1
        self.cam2 = rig.cam2
        self.cam3 = rig.cam3
        self.actuator0 = rig.actuator0
        self.actuator1 = rig.actuator1
        self.screen = rig.screen
        self.imaging = rig.imaging
        self.userdat = Array(c_longdouble,[0]*nuserfields)
        self.userdat_header = ['Unknown']*nuserfields
        self.inQ = rig.inQ
        self.timeout = timeout
        self.address = 'tcp://0.0.0.0:{0}'.format(portnumber)
        self.context = None
        self.socket = None
        self.daemon = True
        
        self.is_stopped.clear()
        if autostart:
            self.start()
        
    def open_context(self):
        if self.context is None:
            self.context = zmq.Context()
            self.socket = self.context.socket(zmq.REP)
            self.socket.bind(self.address)
            self.display('rig-zmq server on address: {0}'.format(self.address))
        
    def run(self):
        while not self.exit.is_set():
            message = None
            if not self.is_stopped.is_set() and self.context is None:
                #open
                self.open_context()
            elif self.is_stopped.is_set() and not self.context is None:
                #close
                self.close_context()
            elif not self.context is None:
                try:
                    message = self.socket.recv_pyobj(flags=zmq.NOBLOCK)
                except zmq.error.Again:
                    continue
                if not message is None:
                    rply = self.parse_message(message)
                    self.socket.send_pyobj(rply)
            else:
                time.sleep(self.timeout*10)
            time.sleep(self.timeout) # maybe this should not be here (check cpu usage).
            
    def parse_message(self,msg):
        rply = dict(type='err',info='unknown')
        if not 'type' in msg.keys():
            rply = dict(type='err',info='message needs "type" key.')
        else:
            if msg['type'] == 'getstate':
                rply = dict(self.get_state(), type = 'state')
            elif msg['type'] == 'cmd':
                if not 'command' in msg.keys():
                    rply = dict(type='err',info='cmd type needs "command" key.')
                else:
                    self.inQ.put(msg['command'])
                    rply = dict(type='ok',info='message sent to rig queue.')
        return rply
            
    def get_state(self):
        out = dict(duetime = self.duinotime.value/1000.,
                    position = self.position/np.array([1000.,1]),
                    laps = self.laps[:]/np.array([1000.,1]),
                    licks = self.licks[:]/np.array([1000.,1]),
                    screen = self.screen[:]/np.array([1000.,1]),
                    imaging = self.imaging[:]/np.array([1000.,1]),
                    cam1 = self.cam1[:]/np.array([1000.,1]),
                    cam2 = self.cam2[:]/np.array([1000.,1]),
                    cam3 = self.cam3[:]/np.array([1000.,1]),
                    reward = self.reward[:]/np.array([1000.,1]),
                    actuator0 = self.actuator0[:]/np.array([1000.,1]),
                    actuator1 = self.actuator1[:]/np.array([1000.,1]))
        # get user data if defined
        for i,k in enumerate(self.userdat_header):
            if not k == 'Unknown':
                out[k] = self.userdat[i]
        return out
    
    def close_context(self):
        if not self.context is None:
            self.display('Stopping rig-zmq server on address: {0}'.format(self.address))

            self.context.destroy()
            self.context = None
        
    def close(self):
        self.exit.set()
        self.close_context()

    def display(self,msg):
        print('['+datetime.today().strftime('%y-%m-%d %H:%M:%S')+'] - ' + msg)


class RigZMQRemote(object):
    # Class to talk to a rig using a separate process through zmq.
    def __init__(self,address, timeout = 0.03):
        self.address = address
        self.context = None
    def open_context(self):
        if self.context is None:
            self.context = zmq.Context()
            self.socket = self.context.socket(zmq.REQ)
            self.socket.connect(self.address)
            self.display('rig-zmq connected to address: {0}'.format(self.address))

    def get_rig_status(self):
        if self.context is None:
            self.open_context()
        self.socket.send_pyobj(dict(type='getstate'))
        message = self.socket.recv_pyobj()
        return message
        
    def close_context(self):
        if not self.context is None:
            self.context.destroy()
            self.context = None
        
    def close(self):
        self.close_context()

    def display(self,msg):
        print('['+datetime.today().strftime('%y-%m-%d %H:%M:%S')+'] - ' + msg)

