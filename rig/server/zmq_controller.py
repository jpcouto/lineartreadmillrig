import time
import zmq
from datetime import datetime
from rig import *
import sys
import numpy as np

def display(msg):
    print('['+datetime.today().strftime('%y-%m-%d %H:%M:%S')+'] - ' + msg)

def zmq_rig_controller(port = '/dev/ttyACM' , portnumber = [0],
                       animalName = 'dummy'):
    socketnumber = '10000{0}'.format(portnumber[0])
    display(socketnumber)
    context = zmq.Context()
    socket = context.socket(zmq.REP)
    socket.bind('tcp://0.0.0.0:{0}'.format(socketnumber))
    port  = [port + str(p) for p in portnumber]

    [display(p) for p in port]
    baudrate = 115200
    

    rig = [TrainingRig(animalName = animalName, port = p) for p in port]
    display('Connected on ' + 'tcp://*:{}'.format(socketnumber))

    expstart = [time.time() for r in rig]
    samplePeriod = 1./60
    # This just runs and starts experiments for the required duration
    # {'action':'name','value':'NAME'} - set the animal name for the rig
    # {'action':'start','value':DURATION} - start an experiment for 1800 seconds
    # {'action':'stop'} - stop experiment
    # {'action':'info'} - return rig variables as py_obj
    stoptime = [-1 for r in rig]
    animalName = [r.getAnimalName() for r in rig]
    default_reward_size = 25
    [r.setRewardSize(default_reward_size) for r in rig]
    while True:
        for r in rig:
            r.update()
        try:
            for i,(r,s) in enumerate(zip(rig,stoptime)):
                if time.time() > s and s > 0:
                    stoptime[i] = -1
                    r.stopExperiment()
                    display('Stopping experiment ({0}).'.format(portnumber[i]))
            #  Wait for next request from client
            time.sleep(samplePeriod)
            try:
                message = socket.recv_pyobj(flags=zmq.NOBLOCK)
            except zmq.error.Again:
                continue
            if message['action'] == 'name':
                i = message['rig']
                animalName[i] = message['value']
                rig[i].setAnimalName(message['value'])
                display('Setting animal '+message['value']+' on rig {0}.'.format(portnumber[i]))
                info = dict(action='name')
                socket.send_pyobj(info)
            elif message['action'] == 'start':
                i = message['rig']                
                rig[i].startExperiment()
                expstart[i] = time.time()
                display(str(message['value']))
                stoptime[i] = time.time() + message['value']
                info = dict(action='start',rig=i)            
                socket.send_pyobj(info)
                display('Started experiment on rig {0}, stopping in {0} min.'.format(
                    portnumber[i],
                    np.round((stoptime[i] - expstart[i])/60.)))
            elif message['action'] == 'rewardsize':
                i = message['rig']                
                display('Reward size [{0}] {1}'.format(animalName[i],message['value']))
                rig[i].setRewardSize(message['value'])
                info = dict(action='done',rig=i)            
                socket.send_pyobj(info)
            elif message['action'] == 'task':
                i = message['rig']                
                display('Task selection [{0}] {1}'.format(animalName[i],message['value']))
                # splits the message
                print(message['value'].split('|'))
                rig[i].initTask(*message['value'].split('|'))
                info = dict(action='done',rig=i)            
                socket.send_pyobj(info)
                
            elif message['action'] == 'logweight':
                i = message['rig']                
                display('Logging {0} weight: {1}'.format(animalName[i],message['value']))
                rig[i].logQ.put('# Animal weight: {0}'.format(message['value']))
                info = dict(action='done',rig=i)            
                socket.send_pyobj(info)
            elif message['action'] == 'stop':
                i = message['rig']                
                stoptime[i] = time.time()
                display('Received stop message.')
                info = dict(action='stop',rig=i)
                socket.send_pyobj(info)
            elif message['action'] == 'info':
                i = message['rig']                
                info = {}
                info['rig'] = i
                info['nlaps'] = int(rig[i].due.laps[1])
                info['time'] = time.time() - expstart[i]
                info['position'] = int(rig[i].due.position[1])
                info['lastReward'] = float(rig[i].due.reward[0])/1000.
                info['lastLick'] = float(rig[i].due.licks[0])/1000.
                info['experimentRunning'] = rig[i].running_experiment.is_set()
                info['animalName'] = animalName[i]
                socket.send_pyobj(info)
            else:
                display('Unknown message ' + message['action'])
        except KeyboardInterrupt:
            display('Keyboard interrupt, shutting down')
            [r.stopExperiment() for r in rig]
            time.sleep(1)
            break
    display('bye')
    [r.close() for r in rig]
    sys.exit()
